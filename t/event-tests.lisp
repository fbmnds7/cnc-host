
(in-package #:cnc-host/event)

(env:log->tcp (format nil "~%-- event tests start -------~%"))

(do-external-symbols (sy :cnc-host/event) (assert sy))

(let ((u (make-usb-msg "g0 x1" "ok"))
      (w (make-woc-xy-msg 0 0))
      (ln (make-line-msg 1 "g0 x1"))
      (g$$ (make-grbl-$$-msg :params :params)))
  (lu:assert-eql u (usb-msg? u))
  (lu:assert-eql w (woc-xy-msg? w))
  (lu:assert-eql ln (line-msg? ln))
  (lu:assert-eql g$$ (grbl-$$-msg? g$$)))

(env:log->tcp (format nil "~%-- event tests end ---------~%"))



