

#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))


#+win32
(load "c:/Users/fboeckh/portacle/all/quicklisp/setup.lisp")
(asdf:require-system :quicklisp)

(ql:quickload 'asdf)
(ql:quickload 'alexandria)
(ql:quickload 'bordeaux-threads)
(ql:quickload 'lparallel)
(ql:quickload 'usocket)
(ql:quickload 'str)
(ql:quickload 'cl-readline)
(ql:quickload 'lisp-unit)
(ql:quickload 'cl-ppcre)
(ql:quickload '(:websocket-driver :websocket-driver-server :clack :woo))

(use-package :websocket-driver)

(defmacro while (test &rest body) `(do () ((not ,test)) ,@body))

(defparameter *ws-send* (lambda(m)(declare (ignore m))()))

(defvar *server* 
  (lambda (env)
    (let ((ws (make-server env)))
      (on :message ws
          (lambda (message)
            (format t "~A~%" message)))
      (on :close ws
          (lambda ()
            (setf *ws-send* (lambda(m)(declare (ignore m))()))))
      (lambda (responder)
        (declare (ignore responder))
        (start-connection ws)
        (setf *ws-send* (lambda (m) (send-text ws m)))
        ;;(while (not (start-connection ws)) (sleep 1)) 
        ;;(while (not (eql :open (ready-state ws))) (sleep 1))
        ))))

(defvar *handler* (clack:clackup *server* :server :woo :port 5000))

(while t
       (sleep 1)
       (funcall *ws-send* (format nil "{\"x\":~A,\"y\":~A}"
                                  (random 500) (random 1000))))

