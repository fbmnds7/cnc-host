
(defpackage #:fsm    (:use #:cl)       (:export #:control!! #:run-repl!! #:%cas))
(defpackage #:repl   (:use #:cl #:fsm) (:export #:test-in-repl))
(defpackage #:usb-sp (:use #:cl #:fsm) (:export #:test-in-usb-sp))

;;; FSM

(in-package #:fsm)

(defmacro %cas (flag old new)
  #+sbcl `(sb-ext:compare-and-swap ,flag ,old ,new)
  #+ecl `(mp:compare-and-swap ,flag ,old ,new))

(defmacro control!! (flag pkg)
  `(lambda () (if (%cas ,flag nil t)
                  (format nil "~A : skip task" ,pkg)
                  (format nil "~A : task run" ,pkg))))


(defmacro control!!! (flag pkg)
  `(if (%cas ,flag nil t)
       (format nil "~A : skip task" ,pkg)
       (format nil "~A : task run" ,pkg)))

(defmacro run-repl!! (flag) `(funcall (control!! ,flag "repl")))

;;(defun run-repl!!! (flag) (control!!! flag "repl"))

;;; REPL

(in-package #:repl)

(defparameter *controller-active* nil)
(defparameter *controller-active-2* nil)
(defun test-in-repl (pkg) (funcall (control!! *controller-active* pkg)))
(assert (string= "repl : task run" (fsm:run-repl!! *controller-active-2*)))
(assert *controller-active-2*)


;;; USB-SP

(in-package #:usb-sp)

(defparameter *controller-active* nil)
(defparameter *controller-active-2* nil)
(defun test-in-usb-sp (pkg) (funcall (control!! usb-sp::*controller-active* pkg)))
(assert (string= "usb-sp : task run" (test-in-usb-sp "usb-sp")))
(assert *controller-active*)

(in-package #:cl-user)

(assert (string= "repl : skip task"   (fsm:run-repl!!        repl::*controller-active-2*)))
(assert (string= "repl : task run"    (repl:test-in-repl     "repl")))
(assert (string= "usb-sp : skip task" (usb-sp:test-in-usb-sp "usb-sp")))


