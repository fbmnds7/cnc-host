
(in-package #:cnc-host/usb-sp)

(env:log->tcp (format nil "~%-- usb-sp tests start ------~%"))

(defvar *res2* "\\$0=10
\\$1=25
\\$2=0
\\$3=7
\\$4=0
\\$5=0
\\$6=0
\\$10=382
\\$11=0.010
\\$12=0.043
\\$13=0
\\$14=0
\\$15=0
\\$16=0
\\$17=0
\\$18=0
\\$19=0
\\$20=0
\\$21=0
\\$22=0
\\$23=0
\\$24=25.000
\\$25=500.000
\\$26=250
\\$27=1.000
\\$28=0.100
\\$29=0
\\$30=1000.
\\$31=0.
\\$32=0
\\$33=5000.000
\\$35=0.000
\\$36=100.000
\\$37=0
\\$39=1
\\$40=0
\\$41=0
\\$42=2
\\$43=1
\\$44=0
\\$45=0
\\$46=0
\\$56=5.000
\\$57=100.000
\\$58=-5.000
\\$59=500.000
\\$60=0
\\$61=0
\\$62=0
\\$63=1
\\$64=0
\\$65=0
\\$70=0
\\$100=320.000
\\$101=324.000
\\$102=320.000
\\$110=1200.000
\\$111=1200.000
\\$112=800.000
\\$120=10.000
\\$121=10.000
\\$122=10.000
\\$130=796.000
\\$131=630.000
\\$132=105.000
Ok")
(setf (aref *res2* 675) #\Newline)

(if (null (usb-sp-lib:usb-port-open))
    (progn
      (assert (null (usb-sp-lib:usb-port-write "$$")))
      (assert (ppcre:scan *res2* (usb-sp-lib:usb-port-read)))

      (let ((c? (%read-grbl-cmd "?" #'%parse-grbl-state)))
        (assert (eql :idle (cd:grbl-state-st c?))))
      (assert (eql :idle (cd:grbl-state-st (%read-grbl-state))))

                                        ;(format sb-sys:*stderr* "~A~%" )

      (assert (eql :sp-ok (usb-sp-lib:usb-port-close))))
    (format t "USB port in use"))

(env:log->tcp (format nil "~%-- usb-sp tests end --------~%"))


