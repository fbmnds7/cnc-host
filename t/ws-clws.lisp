

#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))


#+win32
(load "c:/Users/fboeckh/portacle/all/quicklisp/setup.lisp")
(asdf:require-system :quicklisp)

(ql:quickload 'asdf)
(ql:quickload 'bordeaux-threads)
(ql:quickload 'clws)

(defpackage #:clws-echo
  (:use :cl :clws))

(in-package #:clws-echo)

(setf *protocol-76/00-support* t)
(setf *debug-on-server-errors* t)
(setf *debug-on-resource-errors* t)

(defmacro while (test &rest body) `(do () ((not ,test)) ,@body))

(bordeaux-threads:make-thread (lambda ()
                                (run-server 5000))
                              :name "websockets server")

(defclass echo-resource (ws-resource)
  ((clients :initform () :accessor clients)))

(defmethod resource-client-connected ((res echo-resource) client)
  (format t "got connection on echo server from ~s : ~s~%" (client-host client) (client-port client))
  (push client (clients res))
  t)

(defmethod resource-client-disconnected ((resource echo-resource) client)
  (format t "Client disconnected from resource ~A: ~A~%" resource client))

(defmethod resource-received-text ((res echo-resource) client message)
  (format t "got frame ~s from client ~s" message client)
  (write-to-client-text client message))

(defmethod resource-received-binary((res echo-resource) client message)
  (format t "got binary frame ~s from client ~s" (length message) client)
  (write-to-client-binary client message))

(defvar res1 (make-instance 'echo-resource))

(register-global-resource "/feed"
                          res1
                          (origin-prefix  "http://127.0.0.1"
                                          "file://"
                                          ))

(bordeaux-threads:make-thread (lambda ()
                                (run-resource-listener
                                 (find-global-resource "/feed")))
                              :name "resource listener for /feed")

(while t
       (when (clients res1)
               (write-to-clients-text (clients res1)
                                      (format nil "{\"x\":~A,\"y\":~A}"
                                              (random 500) (random 1000))))
       (sleep 1))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;(register-global-resource "/feed2" res1 nil)

(write-to-clients-text (clients res1)
                       (format nil "{\"x\":~A,\"y\":~A}"
                               (random 500) (random 1000)))


