(in-package #:monad)

(setq *print-failures* t)

(defclass-by-slots float-params (x y z i j k r s f))

(defclass-by-slots staged-cmds (cmds fps))

(defclass maybe-staged-cmds (maybe_t)
  ((monad :initform (make-instance 'staged-cmds))))

(defmethod cmds ((m maybe-staged-cmds)) (pure (monad-fn m 'cmds)))

(defmethod fps ((m maybe-staged-cmds)) (pure (monad-fn m 'fps)))

(defparameter st-cmds (make-instance 'staged-cmds))
(setf (cmds st-cmds) '(:G00))
(setf (fps st-cmds) (make-instance 'float-params))

(defparameter maybe-st-cmds (make-instance 'maybe-staged-cmds))
(setf (monad maybe-st-cmds) st-cmds)

(define-test test-maybe
  (assert-equal '(:G00) (cmds (maybe maybe-st-cmds)))
  (assert-equal '(:G00) (cmds maybe-st-cmds))
  (setf (cmds st-cmds) nil)
  (assert-nil (cmds maybe-st-cmds))
#|  
  (setf (cmds maybe-st-cmds) '(:G00))
  (assert-equal '(:G00) (cmds (maybe maybe-st-cmds)))
  (assert-equal '(:G00) (cmds maybe-st-cmds))
|#
  )

(print-errors (run-tests :all))

