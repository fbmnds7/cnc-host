
(in-package #:cnc-host/db)

(env:log->tcp (format nil "~%-- db tests start ----------~%"))

(defparameter nc-path "~/projects/cnc-host/nc/leg.nc")

(defmethod init-st-acc-fn ()
  (let ((st-acc-0 (make-instance 'stage-acc
                                 :gc-state-0 (make-instance 'gc-state
                                                            :mode :abs))))
    (unless (woc-pos (gc-state-0 st-acc-0))
      (setf (woc-pos (gc-state-0 st-acc-0))
            (make-instance 'point-3 :x 0 :y 0 :z 0)))
    st-acc-0))

(defun log-err-fn (s) (declare (ignore s)) nil)
(defun log-info-fn (s) (declare (ignore s)) nil)
(defun ws-send-fn (s) (declare (ignore s)) nil)

(defun nc-to-db (nc-path)
  (destructuring-bind (nc-path db-path sha256) (db-path nc-path)
    (create-nc-db db-path)
    (insert-file nc-path db-path sha256)
    (let ((line-nr 0)
          (stages '(stage-7 stage-6 stage-5 stage-4 stage-3 stage-2 stage-1))
          (st-acc-0 (init-st-acc-fn)))
      (with-open-file (in nc-path)
        (loop for nc = (read-line in nil)
              while nc do
              (incf line-nr)
              (setf (gc-block st-acc-0) nc)
              (let ((st-acc (§:fold-fns stages st-acc-0)))
                (if st-acc
                    (progn
                      (insert-gcode db-path sha256 line-nr st-acc)
                      (setf st-acc-0
                            (make-instance 'stage-acc
                                           :gc-state-0 (gc-state-0 st-acc))))
                    (return
                      (log-err-fn
                       (format nil "(~A : unexpected error)" nc))))))))))

(format uiop:*stdout* "~%Generating GCode database file for ~A ..." nc-path)
(force-output uiop:*stdout*)
(nc-to-db nc-path)
(format uiop:*stdout* " done.~%")
(force-output uiop:*stdout*)

(with-connection (second (db-path nc-path))
  (let* ((s1 (cadr (member :sha256
                           (first (d:retrieve-all
                                   (sx:select :* (sx:from :nc-file)))))))
         (s2 (first (str:split " " (uiop:run-program
                                    (format nil "sha256sum ~A" nc-path)
                                    :output uiop:*stdout*)))))

    (assert (string= s1 s2))))

(terpri)

(env:log->tcp (format nil "~%-- db tests end ------------~%"))
              


