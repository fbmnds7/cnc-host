
#+win32
(load "c:/Users/fboeckh/portacle/all/quicklisp/setup.lisp")
(asdf:require-system :quicklisp)

(ql:quickload 'asdf)

(ql:quickload 'usocket) 
;;(ql:quickload 'usocket-server)
(add-package-local-nickname :us :usocket)

(defparameter *port* 8082)

(defparameter *bytes* nil)

(defun server-fn (socket)
  (loop 'forever
        (us:wait-for-input socket)
        (us:with-connected-socket (connection (us:socket-accept socket))
          (let ((bytes (read-line (us:socket-stream connection))))
            (format t "~a~%" bytes)
            (setq *bytes* bytes)
            (when (string= bytes ":q")
              (return))))))

(defun start-simple-server (fn port)
  (us:with-socket-listener (socket "127.0.0.1" port)    
    (funcall fn socket)))

(start-simple-server 'server-fn *port*)

(defun client-fn (text)
  (lambda (stream)
    (format stream text)
    (force-output stream)))

(defun start-simple-client (fn port)
  (us:with-client-socket (socket stream "127.0.0.1" port)
    (funcall fn stream)))


(defun bye (stream)
  (format stream ":q")
  (force-output stream))

(dotimes (x 5)
  (start-simple-client (client-fn ":text")  *port*))

(start-simple-client 'bye *port*)

