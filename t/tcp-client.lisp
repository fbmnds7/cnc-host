
#+win32
(load "c:/Users/fboeckh/portacle/all/quicklisp/setup.lisp")
(asdf:require-system :quicklisp)

(ql:quickload 'asdf)

(ql:quickload 'usocket) 
;;(ql:quickload 'usocket-server)
(add-package-local-nickname :us :usocket)

(defparameter *port* 8082)

(defparameter *debug* t)

(defun client-fn (text)
  (lambda (stream)
    (format stream text)
    (force-output stream)))

(defun start-simple-client (fn port)
  (us:with-client-socket (socket stream "127.0.0.1" port)
    (funcall fn stream)))

(defun bye (stream)
  (format stream ":q")
  (force-output stream))

#|
(dotimes (x 5)
  (start-simple-client (client-fn ":text")  *port*))

(start-simple-client 'bye *port*)
|#
