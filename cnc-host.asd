;;;; cnc-host.asd

(asdf:defsystem #:cnc-host
  :description "Describe cnc-host here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on
  (#:asdf
   #:quicklisp
   #:alexandria
   #:bordeaux-threads
   #:lparallel
   #:misc
   #:gcode
   #:usb-sp-lib
   #:usocket
   #:str
   #:cl-readline
   #:lisp-unit
   #:cl-ppcre
   #:hunchensocket 
   #:hunchentoot
   #:sxql
   #:datafly)
  :serial t
  :components
  ((:file "package")
   (:module "src"
    :components
    ((:file "env")
     (:file "event")
     (:file "cqueue")
     (:file "task")
     (:file "control-data")
     (:file "usb-serial")
     (:file "db")
     (:file "ws-feed")
     (:file "file")
#|
     (:file "repl-ws")     
     
     (:file "repl-readline")
     (:file "repl-data")
     (:file "repl-parse")
     (:file "repl")
     (:file "control")
     (:file "cnc-host")
     (:file "run-tests")
     (:file "run")
|#
     ))
   (:module "t"
    :components
    ((:file "event-tests")
     (:file "usb-sp-tests")
     ;(:file "db-tests")
     ))))

