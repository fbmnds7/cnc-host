
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload 'asdf)
(ql:quickload 'alexandria)
(ql:quickload 'bordeaux-threads)
(ql:quickload 'lparallel)
(ql:quickload 'usocket)
(ql:quickload 'str)
(ql:quickload 'cl-readline)
(ql:quickload 'lisp-unit)
(ql:quickload 'cl-ppcre)
(ql:quickload 'hunchensocket) 
(ql:quickload 'hunchentoot)
(ql:quickload 'sxql)
(ql:quickload 'datafly)
(ql:quickload 'cffi)
(ql:quickload 'misc)
(ql:quickload 'gcode)
(ql:quickload 'usb-sp-lib)

(load "package.lisp")

(load "src/env.lisp")

(load "src/event.lisp")

(load "src/cqueue.lisp")

(load "src/task.lisp")

(load "src/db.lisp")

(load "src/control-data.lisp")

(load "src/usb-serial.lisp")

(load "src/ws-feed.lisp")

(load "src/file.lisp")

(in-package #:cnc-host/usb-sp)


(defvar e? (evt:make-event :ctl-0 :?))

(defvar e$$ (evt:make-event :ctl-$ :$$))

(defun push-e-ln (n)
  (dotimes (i n i)
    (let ((e (evt:make-line-msg-event i (format nil "G0 X~a" (* i 10)))))
      (cd:send-event :usb-qu-ln e))))

(defun grbl-ln (s)
  (let ((e (evt:make-line-msg-event 0 s)))
      (cd:send-event :usb-qu-ln e)))

(defvar e-quit (evt:make-event :ctl-0 :quit))
(defun quit-usb ()
  (cd:send-event :usb-qu-0 e-quit)
  (sleep 3)
  (assert (every #'null (lparallel:task-categories-running))))


;(usl:usb-port-open "/dev/ttyACM0" t)

(unless
    (position :usb-task (lparallel:task-categories-running))
  (defvar usb-task (cnc-host/task:make-task :usb-task #'usb-fn))
  (cnc-host/task:run usb-task (cd:usb-chan-ch cd:usb-ch)))

(cd:send-event :usb-qu-0 e?)
;; (sleep (* 3 (/ env:*grbl-gap-tunits* internal-time-units-per-second)))
;; (assert (null (cd:peek-event :usb-chan-qu-0)))
;; (sleep 3)
;; (assert (cd:peek!-event :ctl-chan-qu-0))

(cd:send-event :usb-qu-$ e$$)
;; (sleep 10)
;; (assert (cd:peek!-event :ctl-qu-$))


(in-package #:cnc-host/file)

(defvar file-task (cnc-host/task:make-task :file-task #'file-fn))
(cnc-host/task:run file-task (cd:file-chan-ch cd:file-ch))

(assert (equal (list :FILE-TASK :USB-TASK)
               (sort
                (coerce
                 (remove-if-not #'identity (lparallel:task-categories-running))
                 'list)
                #'string<)))

(defvar nc-path (merge-pathnames "projects/cnc-host/nc/cubic.nc"
                                 (user-homedir-pathname)))

(defvar nc-msg (evt::make-nc-file-msg :type :nc :path nc-path))
(defvar e-nc (evt:make-event :ctl-$ nc-msg))
(cd:send-event :file-qu-$ e-nc)


