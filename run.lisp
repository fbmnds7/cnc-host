(defparameter *debug* t)

(load "run-tests.lisp")

(cnc-host/ws-feed:run-feed-server "/feed" 5000)

(when *debug*
  (cnc-host::main)
  (load "t/tcp-client.lisp"))
