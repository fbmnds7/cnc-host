
(ql:quickload :cserial-port)

(defparameter *s* (open-serial "/dev/ttyACM0" :baud-rate 115200))

(setf *t* (make-serial-stream *s*))

(write-sequence
 (make-array 2 :element-type '(unsigned-byte 8)
             :initial-contents '(36 36)) *t*)

(read-serial-string (make-array 10000 :element-type '(unsigned-byte 8) :adjustable t) *s*)
