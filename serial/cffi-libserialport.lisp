;;(asdf:load-system :cffi)

#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))
(ql:quickload :cffi)

(defpackage :cffi-serialport
  (:use :common-lisp :cffi))

(in-package :cffi-serialport)

(define-foreign-library libserialport
    ;;(:darwin (:or "libserialport.0.dylib" "libserialport.dylib"))
    (:unix (:or "libserialport.so.0" "libserialport.so"))
  (t (:default "libserialport")))

(use-foreign-library libserialport)

#|
134 struct sp_port {
135         char *name;
136         char *description;
137         enum sp_transport transport;
138         int usb_bus;
139         int usb_address;
140         int usb_vid;
141         int usb_pid;
142         char *usb_manufacturer;
143         char *usb_product;
144         char *usb_serial;
145         char *bluetooth_address;
146 #ifdef _WIN32
...
158 #else
159         int fd;
160 #endif
161 };
|#
(cffi:defcstruct (c-sp-port :class sp-port) 
  (name (:pointer :char))
  (description (:pointer :char))
  (sp-transport :int)
  (usb-bus :int)
  (usb-address :int)
  (usb-vid :int)
  (usb-pid :int)
  (usb-manufacturer (:pointer :char))
  (usb-product (:pointer :char))
  (usb-serial (:pointer :char))
  (bluetooth-address (:pointer :char))
  (fd :int))
;;
(cffi:defctype sp-port-ptr (:pointer (:struct c-sp-port)))
(cffi:defctype sp-port-array-ptr (:pointer sp-port-ptr))
;;
;; #include <stdint.h>
;; #include <stdio.h>
;; 
;; #include <libserialport.h>
;; 299
;; 300 /** Return values. */
;; 301 enum sp_return {
;; ... 
(cffi:defcenum sp-return
  (:sp-ok 0)
  (:sp-err-arg  -1)
  (:sp-err-fail -2)
  (:sp-err-mem  -3)
  (:sp-err-supp -4))
;;
;;
;; 314 /** Port access modes. */
;; 315 enum sp_mode {
;; ...
(cffi:defcenum sp-mode
  (:sp-mode-read       1)
  (:sp-mode-write      2)
  (:sp-mode-read-write 3))
;;
;;
;; 344 /** Parity settings. */
;; 345 enum sp_parity {
;; ...
(cffi:defcenum sp-parity
  (:sp-parity-invalid -1)
  (:sp-parity-none     0)
  (:sp-parity-odd      1)
  (:sp-parity-even     2)
  (:sp-parity-mark     3)
  (:sp-parity-space    4))
;;
;;
;; 418 /** Standard flow control combinations. */
;; 419 enum sp_flowcontrol {
;; ...
(cffi:defcenum sp-flowcontrol
  (:sp-flowcontrol-none    0)
  (:sp-flowcontrol-xonxoff 1)
  (:sp-flowcontrol-rtscts  2)
  (:sp-flowcontrol-dtrdsr  3))
;;
;;
;; #define PORT_CONF_FLAGS       SP_MODE_READ_WRITE
;; #define PORT_CONF_BAUDRATE    115200
;; #define PORT_CONF_BITS        8
;; #define PORT_CONF_PARITY      SP_PARITY_NONE
;; #define PORT_CONF_STOPBITS    1
;; #define PORT_CONF_FLOWCONTROL SP_FLOWCONTROL_NONE
;; 
(defconstant +port-conf-flags+       
  (foreign-enum-value 'sp-mode :sp-mode-read-write))
(defconstant +port-conf-baudrate+ 115200)
(defconstant +port-conf-bits+     8)
(defconstant +port-conf-parity+      
  (foreign-enum-value 'sp-parity :sp-parity-none))
(defconstant +port-conf-stopbits+ 1)
(defconstant +port-conf-flowcontrol+ 
  (foreign-enum-value 'sp-flowcontrol :sp-flowcontrol-none))
;; 
;; struct sp_port **port_list = NULL;
;; uint32_t n_ports = 0;
;; struct sp_port *local_list[10] = { NULL };
;; uint32_t local_index = 4;
;; struct sp_port *the_port = NULL;
;; 
;; uint32_t in_bytes = 0;
;; uint8_t in_buf[1024] = { 0x00 };
;; uint32_t in_buf_bytes = 0;
;; uint32_t out_bytes = 0;
;; uint8_t out_buf[128] = "$$\r";
;; uint32_t out_buf_bytes = 3;
;; 
;; sp_list_ports(&port_list) == SP_OK
;;
(cffi:defcfun "sp_list_ports" sp-return (port-list :pointer))
;;
;; 
;; string <- sp_get_port_name(port_list[ii]))
;;
(cffi:defcfun "sp_get_port_name" :string (port :pointer))
;;
;; 
;; int <- (sp_copy_port(port_list[ii], &local_list[ii]))
;;
(cffi:defcfun "sp_copy_port" sp-return (port-src :pointer) (port-dest :pointer))
;;
;; 
;; int <- sp_free_port_list(port_list)
;;
(cffi:defcfun "sp_free_port_list" sp-return (port-list :pointer))
;; 
;;
;; the_port = local_list[local_index];
;; 
;; sp_open(the_port, PORT_CONF_FLAGS) == SP_OK
;;
(cffi:defcfun ("sp_open") sp-return (port :pointer) (port-conf-flags :int))
;;
;; 
;; sp_set_baudrate   (the_port, PORT_CONF_BAUDRATE);
;; sp_set_bits       (the_port, PORT_CONF_BITS);
;; sp_set_parity     (the_port, PORT_CONF_PARITY);
;; sp_set_stopbits   (the_port, PORT_CONF_STOPBITS);
;; sp_set_flowcontrol(the_port, PORT_CONF_FLOWCONTROL);
;;
(cffi:defcfun "sp_set_baudrate" :void
  (port :pointer) 
  (port-conf-baudrate :int))
(cffi:defcfun "sp_set_bits" :void
  (port :pointer) 
  (port-conf-bits :int))
(cffi:defcfun "sp_set_parity" :void
  (port :pointer)
  (port-conf-parity :int))
(cffi:defcfun "sp_set_stopbits" :void
  (port :pointer) 
  (port-conf-stopbits :int))
(cffi:defcfun "sp_set_flowcontrol" :void
  (port :pointer)
  (port-conf-flowcontrol :int))
;;     
;;
;; out_bytes = sp_blocking_write(the_port, out_buf, out_buf_bytes, 1000)
;;
(cffi:defcfun "sp_blocking_write" :int
  (port :pointer)
  (out_buf :string)
  (out_buf_bytes :int)
  (timeout_ms :int))
;;
;; 
;; in_bytes = sp_blocking_read(the_port, in_buf, 1024, 1000)
;; 
(cffi:defcfun "sp_blocking_read" :int
  (port :pointer)
  (in_buf :string)
  (in_buf_bytes :int)
  (timeout_ms :int))
;; 
;;
;; (sp_close(the_port) != SP_OK)
;;
(cffi:defcfun "sp_close" sp-return
  (port :pointer))
;;
;;
;; sp_free_port(the_port)
;;
(cffi:defcfun "sp_free_port" :void
  (port :pointer))

(cffi:defcfun "sp_get_port_by_name" sp-return (name :string) (port :pointer))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




(defparameter *port-list* (cffi:foreign-alloc :pointer))
;; struct sp_port **port_list = NULL;
(setf (cffi:mem-ref *port-list* :pointer) (cffi:null-pointer))

(sp-list-ports *port-list*)

(sp-get-port-by-name "/dev/ttyACM0" *port-list*)

(defparameter *port* (mem-ref *port-list* :pointer))

(sp-open *port* +port-conf-flags+)
(sp-set-baudrate *port* +port-conf-baudrate+)
(sp-set-bits *port* +port-conf-bits+)
(sp-set-parity *port* +port-conf-parity+)
(sp-set-stopbits *port* +port-conf-stopbits+)
(sp-set-flowcontrol *port* +port-conf-flowcontrol+)

(defconstant +in-buf-size+ 1000)
(defconstant +out-buf-size+ 1000)
(defconstant +timeout-ms+ 1000)

(defparameter in-buf (cffi:foreign-string-alloc (make-string +in-buf-size+)))

(defparameter out-buf (cffi:foreign-string-alloc (make-string +out-buf-size+)))
(setf (mem-ref out-buf :char) (char-int #\$))
(setf (mem-ref out-buf :char (cffi:foreign-type-size :char)) (char-int #\$))
(setf (mem-ref out-buf :char (* 2 (cffi:foreign-type-size :char))) 10)

(dotimes (i 20)
  (sp-blocking-write *port* out-buf +out-buf-size+ +timeout-ms+)
  (cffi:lisp-string-to-foreign (make-string +in-buf-size+) in-buf +in-buf-size+)
  (sp-blocking-read *port* in-buf +in-buf-size+ +timeout-ms+)
  (print (cffi:foreign-string-to-lisp in-buf)))

(sp-close *port*)
(sp-free-port *port*)
(sp-free-port-list (mem-ref *port-list* :pointer))





