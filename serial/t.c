/**
 * @file       test_libserialport.c
 * @brief      Basic sigrok's libserialport tests, covering main functionality
 *
 *             This test successfully runs on Debian 9 with FT232BM USB-Serial
 *             adapter, attached to `/dev/ttyUSB0` and detected by this progam
 *             as index 0. Set `local_index` to a different value to test a
 *             different port.
 *
 *             To test if write + read is working, a physical connection
 *             between RX and TX is needed, so an echo channel is created.
 *
 *             With the help of the adapter's LEDs, `write()` is easily tested
 *             (and therefore `open()`), and with the shortcircuit between RX
 *             and TX, `read()` is tested.
 *
 *             Test command:
 *             > # gcc test_libserialport.c -pedantic -lserialport && ./a.out
 *             > gcc t.c -pedantic -I/home/dev/.local/include -L/home/dev/.local/lib -lserialport -o test_libserial
 *             
 *             
 *             TX-RX shortcircuited, success test output:
 *             
 *             ```
 *             1. List ports ...
 *             Found port '/dev/ttyUSB0' .. copied OK
 *             2. Open port by index #0 ...
 *              + Port open and configure OK
 *             3. Writing to port #0 ...
 *              - buffer data: 48656C6C6F20576F726C6421
 *              + Port write OK
 *             4. Read from port #0 ...
 *              - buffer data: 48656C6C6F20576F726C6421
 *              + Port read OK
 *             5. Closing and freeing resources #0 ...
 *              + Port close OK
 *             ```
 *
 *             
 *             TX-RX open, fail test output:
 *             
 *             ```
 *             1. List ports ...
 *             Found port '/dev/ttyUSB0' .. copied OK
 *             2. Open port by index #0 ...
 *              + Port open and configure OK
 *             3. Writing to port #0 ...
 *              - buffer data: 48656C6C6F20576F726C6421
 *              + Port write OK
 *             4. Read from port #0 ...
 *              ! Received 0 bytes, expected 12. Read FAILED
 *             ```
 *             
 * @author     Daniel Narbona Miguel
 * @author     CieNTi
 *
 * @version    v1.0.0
 */
#include <stdint.h>
#include <stdio.h>

#include <libserialport.h>

#define PORT_CONF_FLAGS       SP_MODE_READ_WRITE
#define PORT_CONF_BAUDRATE    115200
#define PORT_CONF_BITS        8
#define PORT_CONF_PARITY      SP_PARITY_NONE
#define PORT_CONF_STOPBITS    1
#define PORT_CONF_FLOWCONTROL SP_FLOWCONTROL_NONE

/* Port-related variables */
struct sp_port **port_list = NULL;
uint32_t n_ports = 0;
struct sp_port *local_list[10] = { NULL };
uint32_t local_index = 4;
struct sp_port *the_port = NULL;

/* Buffer-related variables */
uint32_t in_bytes = 0;
uint8_t in_buf[1024] = { 0x00 };
uint32_t in_buf_bytes = 0;
uint32_t out_bytes = 0;
uint8_t out_buf[128] = "$$\r";
uint32_t out_buf_bytes = 3;

/* Here we go! */
int main(int argc, char *argv[])
{
  uint32_t res = 0;
  uint32_t ii = 0;

  /* 
   * List port, copying to a local list 
   */
  printf("1. List ports ...\n");
  if (sp_list_ports(&port_list) == SP_OK)
  {
    for(ii = 0; port_list[ii]; ii++)
    {
      printf("Found port '%s' .. ", sp_get_port_name(port_list[ii]));
      printf("index '%d' .. ", ii);
      if (!(sp_copy_port(port_list[ii], &local_list[ii])))
      {
        printf("copied OK\n");
        n_ports++;
      }
      else
      {
        printf("copy FAILED !!\n");
        break;
      }
    }
  }
  else
  {
    printf("Port list FAILED !!\n");
    return 1;
  }

  /* Free the list */
  sp_free_port_list(port_list);

  /* 
   * Set a port to open
   */
  the_port = local_list[local_index];

  /* 
   * Open by list index instead name 
   */
  printf("2. Open port by index #%u ...\n", local_index);
  if (sp_open(the_port, PORT_CONF_FLAGS) == SP_OK)
  {
       sp_set_baudrate(the_port, PORT_CONF_BAUDRATE);
           sp_set_bits(the_port, PORT_CONF_BITS);
         sp_set_parity(the_port, PORT_CONF_PARITY);
       sp_set_stopbits(the_port, PORT_CONF_STOPBITS);
    sp_set_flowcontrol(the_port, PORT_CONF_FLOWCONTROL);
    printf(" + Port open and configure OK\n");
  }
  else
  {
    printf(" ! Port open FAILED\n");
  }

  /*
   * Write something to port
   */
  printf("3. Writing to port #%u ...\n - buffer data: ", local_index);
  for (ii = 0; ii < out_buf_bytes; ii++)
  {
    printf("%02X", out_buf[ii]);
  }
  printf("\n");
  out_bytes = sp_blocking_write(the_port, out_buf, out_buf_bytes, 1000);
  if (out_bytes != out_buf_bytes)
  {
    printf(" ! Port write FAILED\n");
    return 1;
  }
  printf(" + Port write OK\n");

  /*
   * Read something from port
   */
  printf("4. Read from port #%u ...\n", local_index);
  //in_bytes = sp_blocking_read_next(the_port, in_buf, 1024, 1000);
  in_bytes = sp_blocking_read(the_port, in_buf, 1024, 1000);
  /*
  if (in_bytes != out_buf_bytes)
  {
    printf(" ! Received %u bytes\n",
           in_bytes,
           out_buf_bytes);
    return 1;
  }
  */
  printf(" - buffer data: ");
  for (ii = 0; ii < in_bytes; ii++)
  {
    //printf("%02X", in_buf[ii]);
    printf("%c", in_buf[ii]);
  }
  printf("\n");
  printf(" + Port read OK\n");

  /*
   * Close and free resources
   */
  printf("5. Closing and freeing resources #%u ...\n", local_index);
  if (sp_close(the_port) != SP_OK)
  {
    printf(" ! Port close FAILED\n");
    return 1;
  }
  printf(" + Port close OK\n");
  sp_free_port(the_port);
}
