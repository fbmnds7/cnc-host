;;;; package.lisp

#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload 'usocket)

#+ccl (progn
        (rename-package 'misc 'misc '(§ m))
        (rename-package 'gcode 'gcode '(gc))
        (rename-package 'lisp-unit 'lisp-unit '(lu))
        (rename-package 'usocket 'usocket '(us))
        (rename-package 'cl-ppcre 'cl-ppcre '(ppcre))
        (rename-package 'lparallel 'lparallel '(lp))
        (rename-package 'lparallel.queue 'lparallel.queue '(lpq))
        (rename-package 'alexandria 'alexandria '(a))
        (rename-package 'datafly 'datafly '(d))
        (rename-package 'sxql 'sxql '(sx))
        (rename-package 'local-time 'local-time '(lt))
        (rename-package 'hunchentoot 'hunchentoot '(ht))
        (rename-package 'hunchensocket 'hunchensocket '(hs))
        (rename-package 'usb-sp-lib 'usb-sp-lib '(usl)))

(defpackage #:cnc-host/env
  (:use #:cl)
  #+ccl (:nicknames env)
  #-ccl (:local-nicknames (#:us #:usocket))
  (:export #:+event-types+
           #:*grbl-retries*
           #:*grbl-gap-sec*
           #:*grbl-gap-tunits*
           #:*grbl-idle-tunits*
           #:*grbl-retry-tunits*
           #:*grbl-error-sec*
           #:*grbl-bf-thresholds*
           #:*grbl-settings-length*
           #:*log-host*
           #:*log-port*
           #:log->tcp
           #:*repl-host*
           #:*repl-port*
           #:*usb-sp-port*
           #:*usb-sp-qu-ln-capacity*
           #:*usb-sp-timeout-tunits*
           #:*ws-client-url*
           #:*N*))

(defpackage #:cnc-host/event
  (:use #:cl)
  #+ccl (:nicknames evt)
  #-ccl (:local-nicknames (#:lu #:lisp-unit)
                          (#:§ #:misc)
                          (#:env #:cnc-host/env))
  (:export #:event
           #:event?
           #:event-p
           #:event-tag
           #:event-ts
           #:event-msg
           #:grbl-$$-msg
           #:grbl-$$-msg?
           #:grbl-$$-msg-p
           #:grbl-$$-msg-params
           #:line-msg
           #:line-msg?
           #:line-msg-p
           #:line-msg-nr
           #:line-msg-text
           #:line-msg-err
           #:line-msg-event?
           #:make-event
           #:make-line-msg
           #:make-line-msg-event
           #:make-grbl-$$-msg
           #:make-usb-msg
           #:make-woc-xy-msg
           #:make-nc-file-msg
           #:nc-file-msg
           #:nc-file-msg?
           #:nc-file-msg-p
           #:nc-file-msg-type
           #:nc-file-msg-path
           #:usb-msg
           #:usb-msg?
           #:usb-msg-p
           #:usb-msg-cmd
           #:usb-msg-res
           #:usb-msg-err
           #:woc-xy-msg
           #:woc-xy-msg?
           #:woc-xy-msg-p
           #:woc-xy-msg-x
           #:woc-xy-msg-y))

(defpackage #:cnc-host/cqueue
  (:use #:cl)
  #+ccl (:nicknames cq)
  #-ccl (:local-nicknames (#:lp #:lparallel)
                          (#:lpq #:lparallel.queue))
  (:export #:cqueue
           #:cqueue-ch
           #:cqueue-qu
           #:cqueue-p
           #:make-cqueue
           #:make-observable
           #:cqcount
           #:cqpeek
           #:cqpeek!
           #:cqpop
           #:cqpush
           #:cq-empty-p
           #:cq-full-p))

(defpackage #:cnc-host/task
  (:use #:cl)
  #+ccl (:nicknames task)
  #-ccl (:local-nicknames (#:lp #:lparallel))
  (:export #:make-task
           #:run
           #:task
           #:task-fn
           #:task-tag))

(defpackage #:cnc-host/db
  (:use #:cl
        #:gcode)
  #+ccl (:nicknames db)
  #-ccl (:local-nicknames (#:§ #:misc)
                          (#:a #:alexandria)
                          (#:d #:datafly)
                          (#:lt #:local-time)
                          (#:str #:str)
                          (#:sx #:sxql)
                          (#:uiop #:uiop))
  (:export #:create-nc-db
           #:db-path
           #:insert-file
           #:insert-gcode))

(defpackage #:cnc-host/control-data
  (:use #:cl)
  #+ccl (:nicknames cd)
  #-ccl (:local-nicknames (#:a #:alexandria)
                          (#:lp #:lparallel)
                          (#:cq #:cnc-host/cqueue)
                          (#:env #:cnc-host/env)
                          (#:evt #:cnc-host/event))
  (:export #:+queue-event-msg-table+
           #:+grbl-open-states+
           #:+grbl-retry-states+
           #:+grbl-alarm-states+
           #:+grbl-error-states+
           #:+grbl-states+
           ;#:+grbl-state-subscriber-fn+
           #:publish-grbl-state
           #:grbl-state
           #:grbl-state-p
           #:grbl-state-st
           #:grbl-state-wpos
           #:grbl-state-bf
           #:grbl-state-fs
           #:grbl-state-flags
           #:grbl-state-ts
           #:grbl-state-substate
           #:grbl-state-err
           #:grbl-state-err-msg
           #:make-grbl-state
           #:open-state?
           #:retry-state?
           #:alarm-state?
           #:error-state?
           #:halt-state?
           #:open-from-state?
           #:retry-from-state?
           #:alarm-from-state?
           #:error-from-state?
           #:switch-on-maybe-grbl-state
           #:observe
           #:update-observable
           #:ctl-ch
           #:ctl-chan-ch
           #:ctl-chan-qu-0
           #:ctl-chan-qu-$
           #:ctl-chan-qu-job
           #:ctl-chan-qu-usb
           #:file-ch
           #:file-chan-ch
           #:file-chan-qu-0
           #:file-chan-qu-$
           #:file-chan-qu-usb
           #:usb-ch
           #:usb-chan-ch
           #:usb-chan-qu-0
           ;; hide #:usb-chan-qu-$
           #:usb-chan-qu-ln
           #:repl-ch
           #:repl-chan-ch
           #:repl-chan-qu-msg
           #:send-event
           #:peek-event
           #:peek!-event
           #:empty-qu-p
           #:full-qu-p
           #:ctl-qu-0
           #:ctl-qu-$
           #:ctl-qu-job
           #:ctl-qu-usb
           #:file-qu-0
           #:file-qu-$
           #:file-qu-usb
           #:usb-qu-0
           #:usb-qu-?
           #:usb-qu-$
           #:usb-qu-ln
           #:repl-qu-0
           #:repl-qu-msg
           #:maybe-grbl-state
           #:grbl-state!
           #:grbl-state?
           #:grbl-state-read-error
           #:grbl-state-update-error
           #:quit-task
           #:switch-state
           #:switch-fn
           #:switch-sub-st))

(defpackage #:cnc-host/usb-sp
  (:use #:cl)
  #+ccl (:nicknames usb)
  #-ccl (:local-nicknames (#:§ #:misc)
                          (#:a #:alexandria)
                          (#:ppcre #:cl-ppcre)
                          (#:str #:str)
                          (#:uiop #:uiop)
                          (#:usl #:usb-sp-lib)
                          (#:cd #:cnc-host/control-data)
                          (#:env #:cnc-host/env)
                          (#:evt #:cnc-host/event))
  (:export #:*controller-active*
           #:+grbl-fast-states+
           #:read-grbl-state
           #:run
           #:start-usb-sp
           #:usb-sp
           #:usb-sp-push
           #:usb-sp-push-prio))

(defpackage #:cnc-host/ws-feed
  (:use #:cl
        #:hunchentoot
        #:hunchensocket
        #:gcode)
  #+ccl (:nicknames ws)
  #-ccl (:local-nicknames (#:ht #:hunchentoot)
                          (#:hs #:hunchensocket))
  (:export #:run-feed-server
           #:ws-send))

(defpackage #:cnc-host/file
  (:use #:cl)
  #+ccl (:nicknames "file")
  #-ccl (:local-nicknames (#:§ #:misc)
                          (#:gc #:gcode)
                          (#:cd #:cnc-host/control-data)
                          (#:db #:cnc-host/db)
                          (#:env #:cnc-host/env)
                          (#:evt #:cnc-host/event)
                          (#:usb #:cnc-host/usb-sp)
                          (#:ws #:cnc-host/ws-feed))
  (:import-from #:str
                #:from-file
                #:lines))

(defpackage #:cnc-host/repl-rl
  (:use #:cl
	#:str
	#:cl-readline
	#:cnc-host/control-data
	#:cnc-host/cqueue
        #:cnc-host/env
	#:gcode)
  (:export #:run-rl))

(defpackage #:cnc-host/repl
  (:use #:cl
        #:misc
        #:cnc-host/control-data
        #:cnc-host/cqueue
        #:cnc-host/db
        #:cnc-host/env
        #:cnc-host/event
	#:gcode
	#:cnc-host/repl-rl
        #:cnc-host/usb-sp)
  (:import-from #:lparallel
		#:make-channel
                #:submit-task)
  (:import-from #:lparallel.queue
		#:make-queue
                #:queue
		#:push-queue
		#:pop-queue)
  (:import-from #:usocket
		#:socket-stream
		#:socket-accept
		#:with-socket-listener
		#:wait-for-input
		#:with-connected-socket)
  (:import-from #:str
		#:emptyp
		#:replace-all
		#:substring
		#:starts-with?
                #:s-first
                #:s-last
                #:s-rest
		#:trim
		#:upcase)
  (:export #:*controller-active*
           #:repl
           #:dispatch-fn
           #:control-fn
           #:process-fn
           #:run
           #:start-repl-tcp))

(defpackage #:cnc-host/control
  (:use #:cl
        #:cnc-host/control-data
        #:cnc-host/cqueue
        #:cnc-host/env))

(defpackage #:cnc-host
  (:use #:cl
        #:lparallel
        #:lparallel.queue
        #:cnc-host/control-data
        #:cnc-host/cqueue
        #:cnc-host/env
        #:cnc-host/repl
        #:cnc-host/usb-sp))
