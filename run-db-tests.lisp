#!/bin/sh
#|
exec sbcl --script "$0" "$@"
|#


#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload :str)
(ql:quickload :uiop)
(ql:quickload :sxql)
(ql:quickload :datafly)
(ql:quickload :local-time)
(ql:quickload :misc)
(ql:quickload :gcode)

(defpackage #:cnc-host/db
  (:use #:cl
        #:gcode)
  (:local-nicknames (#:§ #:misc)
                    (#:a #:alexandria)
                    (#:d #:datafly)
                    (#:lt #:local-time)
                    (#:str #:str)
                    (#:sx #:sxql)
                    (#:uiop #:uiop))
  (:export #:create-nc-db
           #:db-path
           #:insert-file
           #:insert-gcode))

(load "~/projects/cnc-host/src/db.lisp")
(load "~/projects/cnc-host/t/db-tests.lisp")
   

