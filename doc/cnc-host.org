;; event -> push to subscribed event queue
;; -> task-dispatch 
;; -> select set-handler by (task priority, event priority) 
;; -> handler event = on-event
;; -> emitt event


; task-dispatch
;; priority-queues for events
;; registered tasks with priorities
;; access to task subscriptions


(defgeneric run (task-scheduler))
 
(defclass task-scheduler ()
  ((priority-queue
    :initarg :priority-queue
    :initform '()
    :accessor priority-queue)
   (event-queue
    :initarg :event-queue
    :initform '()
    :accessor event-queue)
   (tasks
    :initarg :task
    :initform '(:active-task
		:idle-task)
    :accessor task)
   (current-task
    :accessor current-task)))

(defmethod run (task-scheduler)
 ; pop event
 ; push to queues of subscribed tasks
 ; schedule (highest priority, not :yield) task 
 ; -> each task goes :yield semaphore

;; read task highest priority

(defgeneric run (active-task))

(defclass active-task ()
  ((priority
    :initarg :priority
    :initform :normal
    :reader :priority)
   (subscribed-events
    :initarg :subscribed-events
    :accessor subscribed-events)
   (active-events
    :initarg :active-events
    :initform '(:init 
		:wait-for-grbl
		:idle 
		:process-cnc-file 
		:recover-from-reset
		:exit)
    :accessor active-events)
   (yield?
    :initarg :yield
    :initform nil
    :accessor yield?)))

; fsm => active-task
(defmethod run (active-task e)
  (cond ((eq :init e) 
	 (on-init fsm))
	((eq :wait-for-grbl e)
	 (on-wait-for-grbl fsm))
	((eq :idle e) 
	 (on-idle fsm))
	((eq :process-cnc-file e) 
	 (on-process-cnc-file fsm))
	((eq :recover-from-reset e) 
	 (on-recover-from-reset fsm))
	(t 
	 (on-yield fsm)
	 (return)))
  (run task-scheduler))

(defmethod on-yield (active-task)
  (setf (slot-value active-task 'yield?) 't)
  (setf (slot-value active-task 'active-events) '()))


;; rtc-tasks
;;; init-task
;;; active-task
;;; recovery-from-reset

;; parallel tasks
;;; task-scheduler
;;; repl
:;; ui
;;; alarm
;;; idle

