
(in-package #:cnc-host/usb-sp)

(define-condition wait-for-wpos-error (error) ())

(defun %wait-for-alarm-wpos ()
  (sleep 0.5)
  (let ((st (%read-grbl-state))
        (i 0))
    (until (or (> i 50) (when (cd:grbl-state-p st) (eq :alarm (cd:grbl-state-st st))))
      ;;(when (equal st (list nil (list nil 0))) (signal 'wait-for-wpos-error))
      (setf st (cd:grbl-state? (%read-grbl-state)))
      (sleep 0.2)
      (incf i))
    (if st
        (cd:grbl-state-wpos st)
        (signal 'wait-for-wpos-error))))

(defun %wait-for-idle-wpos ()
  (sleep 0.5)
  (let ((st (%read-grbl-state))
        (i 0))
    (until (or (> i 50) (when (cd:grbl-state-p st) (eq :idle (cd:grbl-state-st st))))
      (when (and (cd:grbl-state-p st) (eq :alarm (cd:grbl-state-st st)))
        (signal 'wait-for-wpos-error))
      (when (equal st (list nil (list nil 0)))
        (signal 'wait-for-wpos-error))
      (setf st (cd:grbl-state? (%read-grbl-state)))
      (sleep 0.2)
      (incf i))
    (if st
        (cd:grbl-state-wpos st)
        (signal 'wait-for-wpos-error))))


(defmacro %backtrack (axis offset)
  `(progn
     (%move-to ,axis ,offset)
     (sleep (* 4 env:*grbl-gap-sec*))
     (raw-%)
     (sleep (* 4 env:*grbl-gap-sec*))
     (%move-to ,axis ,offset)
     (raw-%)
     (sleep (* 4 env:*grbl-gap-sec*))
     (raw-?)))

(defun homing ()
  (ignore-errors
   (let ((pos (make-list 0)))
     (push (cons :xyz (raw-?)) pos)
     (push (cons :z (%move-to :z 180)) pos)
     (%backtrack :z -7)
     (push (cons :y (%move-to :y -600)) pos)
     (%backtrack :y 10)
     (push (cons :x (%move-to :x -800)) pos)
     (%backtrack :x 10)
     (%backtrack :x 0)
     (mapcar (lambda (x) (/ (round x 0.001D0) 1000D0))
             (list
              (- -10 (car (cd:grbl-state-wpos (cdr (assoc :x pos)))))
              (- -10 (cadr (cd:grbl-state-wpos (cdr (assoc :y pos)))))
              (let ((z0 (caddr (cd:grbl-state-wpos (cdr (assoc :xyz pos)))))
                    (z1 (caddr (cd:grbl-state-wpos (cdr (assoc :z pos))))))
                (if (< z0 0)
                    (- z1 z0 7)
                    (- (+ z0 7) z1))))))))



(progn
  (%move-to :z 180)
  (%backtrack :z -7)
  (%move-to :y -600)
  (%backtrack :y 10)
  (%move-to :x -800)
  (%backtrack :x 10)
  (%backtrack :x 0))



(defvar st1 "[MSG:ok]
<Idle|WPos:50.000,50.000,0.000|Bf:35,1009|FS:0.,0.|Pn:PRHS>
Ok
Ok
Ok
Ok
Ok
Ok
")

(defvar st2 " <Alarm|WPos:0.000,0.000,0.000|Bf:35,1021|FS:0.,0.|Pn:PXYZRHS|WCO:0.000,0.000,0.000|Ov:100,100,100>")

(defmacro %stage-1 (st-s)
  (let ((s (gensym)))
    `(mapcar #'(lambda (,s) (str:split ":" ,s))
             (str:split "|"
                        (first (str:split ">"
                                          (second (str:split "<" ,st-s))))))))

(defmacro %st (st-1)
  `(§:make-upcase-keyword
     (remove-if #'(lambda (c) (eql c #\:))
                (caar ,st-1))))

(defmacro %%parse-floats (pat st-1)
  `(ignore-errors
    (mapcar #'uiop:safe-read-from-string
            (str:split "," (cadr (assoc ,pat ,st-1 :test #'string=))))))

(defmacro %stage-2 (st-s)
  (let ((pats (gensym))
        (st-1 (gensym))
        (st (gensym))
        (flags (gensym))
        (c (gensym))
        (s (gensym)))
    `(ignore-errors
      (let* ((,pats (list "WPos" "Bf" "FS" "Ov" "WCO"))
             (,st-1 (%stage-1 ,st-s))
             (,st (§:make-upcase-keyword
                    (remove-if #'(lambda (,c) (eql ,c #\:)) (caar ,st-1))))
             (,flags (car
                      (str:split ":"
                                 (cadr
                                  (assoc "Pn" ,st-1 :test #'string=))))))
        (append (list (list :st ,st) (list :flags ,flags))
                (mapcar (lambda (,s) (list (§:make-upcase-keyword ,s)
                                           (%%parse-floats ,s ,st-1)))
                        ,pats))))))

(defmacro %wpos (st-1)
  `(mapcar #'uiop:safe-read-from-string
           (str:split "," (cadr (assoc "WPos" ,st-1 :test #'string=)))))



(defmacro %bf (st-1)
  `(mapcar #'uiop:safe-read-from-string
           (str:split "," (cadr (assoc "Bf" ,st-1 :test #'string=)))))


(defmacro %fs (st-1)
  `(mapcar #'uiop:safe-read-from-string
           (str:split "," (cadr (assoc "FS" ,st-1 :test #'string=)))))



