
// Esempio di riferimento per il funzionamento
//http://bl.ocks.org/d3noob/6bd13f974d6516f3e491

// d3js 
// Set the dimensions and margins of the graph
var margin = {top: 20, right: 20, bottom: 30, left: 50},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;



//websocket get new data on close and when
var ws = new WebSocket("ws://127.0.0.1:5000/feed");
ws.onmessage = function (event)
{
    var messages = document.getElementsByTagName('ul')[0],
        message = document.createElement('li'),
        content = document.createTextNode(event.data);
    var o = jQuery.parseJSON(event.data);
    var b = {"x" : o.x, "y" : (height - o.y)};
    console.log(b);
    data2.push(b);
    console.log(data2);
    updateLine2(data2);
};


// Preparo la prima sequenza di dati

var data2 = [
    {"x": 0,"y": height}
];


// Set the ranges
var x = d3.scaleLinear().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);

// Define the axes
var xAxis = d3.axisBottom(x);
    //.ticks(d3.timeMinute.every(30));

var yAxis = d3.axisLeft(y);

// Define the line
/*
var valueline = d3.line()
    .x(function(d) { return x(d.date); })
    .y(function(d) { return y(d.close); });
*/
// append the svg object to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("body")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");



function drawLine2(line_data)
{
    // Scale the range of the data
    x.domain([0, d3.max(line_data, function(d) { return x(d.x); })]);
    y.domain([0, d3.max(line_data, function(d) { return y(d.y); })]);

    
    var value_line = d3.line()
        .x(function(d) { return x(d.x); })
        .y(function(d) { return y(d.y); });


    console.log(value_line);
    
    // Add the valueline path.
    svg.append("path")
        .attr("class", "line")
        .attr("d", value_line(line_data));

    // Add the X Axis
    svg.append("g")
        .attr("class", "x axis")
    //.attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    // Add the Y Axis
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

}

// funzione per aggiornamento
function updateLine2(line_data)
{
    // Scale the range of the data
    x.domain(d3.extent(line_data, function(d) { return d.date; }));
    y.domain([0, d3.max(line_data, function(d) { return d.close; })]);

    // Select the section we want to apply our changes to
    var svg = d3.select("body").transition();

    var value_line = d3.line()
        .x(function(d) { return d.x; })
        .y(function(d) { return d.y; });
    
    // Make the changes
    svg.select(".line")   // change the line
        .duration(250)
        .attr("d", value_line(line_data));

    svg.select(".x.axis") // change the x axis
        .duration(250)
        .call(xAxis);
    svg.select(".y.axis") // change the y axis
        .duration(250)
        .call(yAxis);

}

// Disegno i primi dati
//drawLine(data);
drawLine2(data2);
