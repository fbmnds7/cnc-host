#!/usr/bin/env python3
import asyncio
import datetime
import random
import websockets
import numpy as np
import json

IP = '127.0.0.1'
PORT = 5000


def format_time():
    t = datetime.datetime.now().time()
    s = t.strftime('%H:%M:%S.%f')
    return s[:-3]

async def time(websocket, path):
    while True:
        #now = datetime.datetime.utcnow().isoformat() + 'Z'
        data = {'x': str(random.randint(100,1000)),
                'y': str(random.randint(0,500))}
        await websocket.send(json.dumps(data))
        await asyncio.sleep(random.random() * 3)

start_server = websockets.serve(time, IP, PORT)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
