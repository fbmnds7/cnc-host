#!/bin/sh
#|
exec sbcl --script "$0" "$@"
|#


#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(asdf:require-system :quicklisp)

(ql:quickload 'asdf)
(ql:quickload 'alexandria)
(ql:quickload 'bordeaux-threads)
(ql:quickload 'lparallel)
(ql:quickload 'usocket)
(ql:quickload 'str)
(ql:quickload 'cl-readline)
(ql:quickload 'lisp-unit)
(ql:quickload 'cl-ppcre)
(ql:quickload 'hunchensocket) 
(ql:quickload 'hunchentoot)
(ql:quickload 'sxql)
(ql:quickload 'datafly)
(ql:quickload 'cffi)
(ql:quickload 'misc)
(ql:quickload 'gcode)
(ql:quickload 'usb-sp-lib)

(load "package.lisp")

(load "src/env.lisp")

(load "src/event.lisp")

(load "src/cqueue.lisp")

(load "src/task.lisp")

(load "src/db.lisp")

(load "src/control-data.lisp")

(in-package :cnc-host/usb-sp)

(load "src/usb-serial.lisp")

#|
(load "src/ws-feed.lisp")

(load "src/file.lisp")

(load "src/repl-readline.lisp")
(load "src/repl-data.lisp")
(load "src/repl-parse.lisp")
(load "src/repl.lisp")

(load "src/control.lisp")

(load "src/cnc-host.lisp")
|#

(load "t/event-tests.lisp")
(load "t/usb-sp-tests.lisp")
(load "t/db-tests.lisp")

#|
(when *debug*
  (cnc-host:main)
  (load "t/tcp-client.lisp"))
|#

#+ecl (uiop:quit)

