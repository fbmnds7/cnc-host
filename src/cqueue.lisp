
(in-package #:cnc-host/cqueue)

(defstruct (cqueue
            (:constructor %make-cqueue (&key qu ch))
            :predicate)
  qu ch)

(defun make-cqueue (chan &optional capacity)
  (if capacity
      (%make-cqueue :qu (lpq:make-queue :fixed-capacity capacity) :ch chan)
      (%make-cqueue :qu (lpq:make-queue) :ch chan)))

(defun cqpeek (cq)
  (lpq:peek-queue (cqueue-qu cq)))

(defun cqpeek! (cq) 
  (when (lpq:peek-queue (cqueue-qu cq))
    (lpq:pop-queue (cqueue-qu cq))))

(defun cqpop (cq)
  (lpq:pop-queue (cqueue-qu cq)))

(defun cqpush (cq e)
  (when cq
    (assert (cqueue-p cq))
    (lpq:push-queue e (cqueue-qu cq))))

(defun cqcount (cq)
  (when cq
    (assert (cqueue-p cq))
    (lpq:queue-count (cqueue-qu cq))))

(defun cq-empty-p (cq)
  (when cq
    (assert (cqueue-p cq))
    (lpq:queue-empty-p (cqueue-qu cq))))

(defun cq-full-p (cq)
  (when cq
    (assert (cqueue-p cq))
    (lpq:queue-full-p (cqueue-qu cq))))




