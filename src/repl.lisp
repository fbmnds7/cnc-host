(in-package #:cnc-host/repl)


(defun parse-repl-text (text pre)
  (let ((text (string-trim " " text)))
    (cond ((string= ":q" text)
           (cqpush *ctl-$-queue* (make-event :quit)))
          ((string= ":a" text)
           (cqpush *ctl-0-queue* (make-event :ctl-0)))
          ((string= ":spr+" text)
           (cqpush *ctl-$-queue* (make-event :ctl-0 :spr+)))
          ((starts-with? ":spw+ " text)
           (cqpush *ctl-0-queue*
                 (make-event :ctl-0
                             (list :spw+
                                   (substring (length ":spw+ ") nil text)))))
          ((starts-with? ":spw " text)
           (cqpush *ctl-$-queue*
                 (make-event :ctl-$
                             (list :spw
                                   (substring (length ":spw ") nil text)))))
          ((starts-with? ":f2" text)
           (setf (unit *gc-state*) :mm)
           (setf (mode *gc-state*) :abs)
           (cqpush *ctl-job-queue*
                 (make-event :ctl-job "~/projects/cnc-host/nc/leg.nc")))
          ((starts-with? ":f" text)
           (setf (unit *gc-state*) :mm)
           (setf (mode *gc-state*) :abs)
           "~/projects/cnc-host/nc/cubic.nc"
           (cqpush *ctl-job-queue*
                 (make-event :ctl-job "~/projects/cnc-host/nc/leg.nc")))
          (t nil))))

(defun repl-fn ()
  (lambda ()
    (do ((i 0 (1+ i)))
        ((null :never))
      (run-rl #'parse-repl-text i))))

(defun repl-msg-fn ()
  (lambda ()
    (do ((e (cqpop *repl-msg-queue*) (cqpop *repl-msg-queue*)))
        ((null :never))
      (when e (format t "~&~A~&" e)))))

(defun start-repl-tcp ()
  (with-socket-listener (socket *repl-host* *repl-port*)    
    (let ((pre "cnc-host/repl:repl-server-fn"))
      (loop 'forever
            (wait-for-input socket)
            (with-connected-socket (connection (socket-accept socket))
              (let ((bytes (read-line (socket-stream connection))))
                (error "TODO use repl-parse-text")))))))

