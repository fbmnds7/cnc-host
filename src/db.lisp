
(in-package #:cnc-host/db)

;;(plan 16)


(defun sha-256 (path) (§:sha256-file path))

(defun db-path (nc-path)
  (let* ((nc-path-str (namestring nc-path))
         (nc-path-str (if (str:starts-with? "~" nc-path-str)
                          (str:concat (uiop:getenv "HOME")
                                      (str:substring 1 nil nc-path))
                          nc-path-str)))
    (if (and (uiop:file-exists-p nc-path)
               (str:ends-with? ".nc" nc-path-str))
      (list nc-path-str
            (car (str:add-suffix (list (string-right-trim
                                        "nc" nc-path-str)) "db"))
            (sha-256 nc-path-str))
      (list nil nil nil))))

(defmacro with-connection (db-path &body body)
  `(let ((d:*connection* (d:connect-cached :sqlite3 :database-name ,db-path)))
     ,@body))

(defun create-nc-db (db-path)
  (uiop:delete-file-if-exists db-path)
  (with-connection db-path
    (d:execute
     (sx:create-table :nc-file
                      ((sha256 :type :char64
                               :primary-key t)
                       (path :type '(:varchar 1024))
                       (created_at :type :datetime))))
    (d:execute
     (sx:create-table :nc-line
                      ((line-nr :type :integer
                                :primary-key t)
                       (gc-block :type '(:varchar 128))
                       (sha256 :type :char64)
                       (unit :type '(:varchar 4))
                       (motion :type '(:varchar 4))
                       (mode :type '(:varchar 4))
                       (woc-pos-x :type :real)
                       (woc-pos-y :type :real)
                       (woc-pos-z :type :real))))
    (d:execute
     (sx:create-table :gc-cmds
                      ((key :type :integer
                            :primary-key t)
                       (line-nr :type :integer)
                       (seq-nr :type :integer)
                       (unit :type '(:varchar 4))
                       (gc-type :type '(:varchar 16))
                       (gc-text :type '(:varchar 256))
                       (gc-list :type '(:varchar 64))
                       (x :type :real)
                       (y :type :real)
                       (z :type :real)          
                       (i :type :real)
                       (j :type :real)
                       (k :type :real)
                       (r :type :real)
                       (f :type :real)
                       (s :type :real))))))

;; CNC-HOST/DB> (with-connection (db-path nc-path) (retrieve-all (select :* (from :nc-file))))
;; ((:SHA256
;;   #(53 126 119 33 35 160 76 214 223 46 175 242 80 236 162 213 6 93 234 108 45
;;     233 136 218 16 230 241 85 42 222 253 116)
;;   :PATH "/home/dev/projects/cnc-host/nc/leg.nc" :CREATED-AT
;;   "2020-05-11T09:28:05.239426+02:00"))

(defun insert-file (nc-path db-path sha256)
  (with-connection db-path
    (d:execute
     (sx:insert-into :nc-file
                     (sx:set= :sha256 sha256
                              :path nc-path
                              :created_at (princ-to-string (lt:now)))))))

(defmacro execute-insert-gc-cmd (gc-cmd line-nr seq-nr)
  `(when ,gc-cmd 
     (if (typep ,gc-cmd 'gc-cmd)
         (d:execute
          (sx:insert-into :gc-cmds
                          (sx:set= :line-nr ,line-nr
                                   :seq-nr ,seq-nr
                                   :gc-type (gc-type ,gc-cmd)
                                   :gc-text (gc-text ,gc-cmd)
                                   :gc-list (string-trim
                                             '(#\( #\))
                                             (format nil
                                                     "~a" (gc-list ,gc-cmd)))
                                   :x (x (f-params ,gc-cmd))
                                   :y (y (f-params ,gc-cmd))
                                   :z (z (f-params ,gc-cmd))
                                   :i (i (f-params ,gc-cmd))
                                   :j (j (f-params ,gc-cmd))
                                   :k (k (f-params ,gc-cmd))
                                   :r (r (f-params ,gc-cmd))
                                   :f (f (f-params ,gc-cmd))
                                   :s (s (f-params ,gc-cmd)))))
         (d:execute
          (sx:insert-into :gc-cmds
                          (sx:set= :line-nr ,line-nr
                                   :seq-nr ,seq-nr
                                   :gc-type (gc-type ,gc-cmd)
                                   :gc-text (gc-text ,gc-cmd)))))))

(defun insert-gcode (db-path sha256 line-nr st-acc)
  (when st-acc
    (let ((gc-state-0 (gc-state-0 st-acc))
          (cmds (remove-if (lambda (cmd) (or (null cmd)
                                             (eql :gc-nil (gc-type cmd))))
                           (list (pre-cmd st-acc)
                                 (main-cmd st-acc)
                                 (post-cmd st-acc)))))
      (with-connection db-path
        (d:execute
         (sx:insert-into :nc-line
                         (sx:set= :line-nr line-nr
                                  :gc-block (gc-block st-acc)
                                  :sha256 sha256
                                  :unit (unit gc-state-0)
                                  :motion (motion gc-state-0)
                                  :mode (mode gc-state-0)
                                  :woc-pos-x (x (woc-pos gc-state-0))
                                  :woc-pos-y (y (woc-pos gc-state-0))
                                  :woc-pos-z (z (woc-pos gc-state-0)))))
        (if cmds
            (loop for cmd in cmds do (execute-insert-gc-cmd cmd line-nr 0))
            (execute-insert-gc-cmd (pre-cmd st-acc) line-nr 0))))))

(d:defmodel (nc-file (:inflate created-at #'d:datetime-to-timestamp)
                     (:has-many (nc-lines nc-line)
                                (sx:select :*
                                           (sx:from :nc-line)
                                           (sx:where (:= :sha256 sha256))
                                           (sx:order-by (:incr :sha256))))
                     (:has-a (repl-gc-state-0 gc-state-0)
                             (sx:select :1
                                        (sx:from :gc-state-0)
                                        (sx:where (:= :sha256 sha256)))))
  sha256
  path
  created-at)

(d:defmodel (nc-line (:has-many (pre-cmd gc-cmd)
                                (sx:select :*
                                           (sx:from :gc-cmds)
                                           (sx:where (:= :line-nr line-nr))
                                           (sx:order-by (:incr :seq-nr))))
                     (:has-many (main-cmd gc-cmd)
                                (sx:select :*
                                           (sx:from :gc-cmds)
                                           (sx:where (:= :line-nr line-nr))
                                           (sx:order-by (:incr :seq-nr))))
                     (:has-many (post-cmd gc-cmd)
                                (sx:select :*
                                           (sx:from :gc-cmds)
                                           (sx:where (:= :line-nr line-nr))
                                           (sx:order-by (:incr :seq-nr)))))
  line-nr
  gc-block
  sha256
  unit
  motion
  mode
  woc-pos-x
  woc-pos-y
  woc-pos-z)

(d:defmodel (gc-cmds (:inflate gc-type #'d:string-to-keyword))
  key
  line-nr
  seq-nr
  gc-type
  gc-text
  gc-list
  x
  y
  z
  i
  j
  k
  r
  f
  s)

;;(finalize

