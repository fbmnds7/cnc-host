
(in-package :cnc-host/usb-sp)

(defmacro promised (form)
  (let ((p (gensym)))
    `(progn
       (let ((,p (lparallel:promise)))
         (lparallel:fulfill ,p ,form)
         (lparallel:force ,p)))))

(defmacro while (test &rest body) `(do () ((not ,test)) ,@body))
(defmacro until (test &rest body) `(do () (,test) ,@body))
(defmacro aif (test-form then-form &optional else-form)
  `(let ((it ,test-form)) (if it ,then-form ,else-form)))
(defmacro awhen (test-form &body body)
  `(aif ,test-form (progn ,@body)))

(defvar +grbl-fast-states+  '(:run :jog :error))
(defvar +grbl-params+
  (mapcan #'(lambda (l) (loop for i from (car l) to (cadr l) collect i))
          (list '(1 46) '(56 65) '(70 70)
                '(100 102) '(110 112) '(120 122) '(130 132))))

(defvar *usb-fn* nil)

(defvar *ts-switch-to-open* nil)
(defvar *ts-switch-to-close* nil)
(defvar *ts-switch-to-halt* nil)
(defvar *ts-switch-to-retry* nil)
(defvar *ts-switch-to-alarm* nil)
(defvar *ts-switch-to-error* nil)

(defun reset-usb ()
  (error "undefined")
  ;; TODO check/reset cnc-host/usb-sp-lib:*port*/*port-list* status
  (setf usl:*port-status* nil))

(defun reset-ts (st)
  (setf *ts-switch-to-open*  nil)
  (setf *ts-switch-to-halt*  nil)
  (setf *ts-switch-to-alarm* nil)
  (setf *ts-switch-to-retry* nil)
  (setf *ts-switch-to-error* nil)
  (let ((ts (get-internal-real-time)))
    (case st
      (:open  (setf *ts-switch-to-open*  ts))
      (:halt  (setf *ts-switch-to-open*  ts))
      (:alarm (setf *ts-switch-to-alarm* ts))
      (:retry (setf *ts-switch-to-retry* ts))
      (:error (setf *ts-switch-to-error* ts))
      (otherwise
       (error (format nil "undefined state identifier ~a" st))))))

(defmacro %make-error-grbl-state (err)
  `(cd:make-grbl-state
    :st :error                          ; asserted error state
    :wpos nil :bf nil :fs nil :flags nil :substate nil :err-msg ,err))

(defmacro %parse-floats (pat st-1)
  `(ignore-errors
    (mapcar #'uiop:safe-read-from-string
            (str:split "," (cadr (assoc ,pat ,st-1 :test #'string=))))))

(defmacro %%value (i l &optional (test #'eql)) `(cadr (assoc ,i ,l :test ,test)))

(defmacro %%stage-1 (st-s)
  (let ((s (gensym)))
    `(mapcar #'(lambda (,s) (str:split ":" ,s))
             (str:split "|"
                        (first (str:split ">"
                                          (second (str:split "<" ,st-s))))))))

(defmacro %%stage-2 (st-s)
  (let ((pats (gensym))
        (st-1 (gensym))
        (st (gensym))
        (flags (gensym))
        (c (gensym))
        (s (gensym)))
    `(let* ((,pats (list "WPos" "Bf" "FS" "Ov" "WCO"))
            (,st-1 (%%stage-1 ,st-s))
            (,st (§:make-upcase-keyword
                   (remove-if #'(lambda (,c) (eql ,c #\:)) (caar ,st-1))))
            (,flags (car
                     (str:split ":" (%%value "Pn" ,st-1 #'string=)))))
       (append (list (list :st ,st) (list :flags ,flags))
               (mapcar (lambda (,s) (list (§:make-upcase-keyword ,s)
                                          (%parse-floats ,s ,st-1)))
                       ,pats)))))

(defmacro %parse-grbl-state (s)
  "https://github.com/gnea/grbl/wiki/Grbl-v1.1-Interface
Machine State:

Valid states types: Idle, Run, Hold, Jog, Alarm, Door, Check, Home, Sleep
Sub-states may be included via : a colon delimiter and numeric code.

Current sub-states are:
- `Hold:0` Hold complete. Ready to resume.
- `Hold:1` Hold in-progress. Reset will throw an alarm.
- `Door:0` Door closed. Ready to resume.
- `Door:1` Machine stopped. Door still ajar. Can't resume until closed.
- `Door:2` Door opened. Hold (or parking retract) in-progress. Reset will throw an alarm.
- `Door:3` Door closed and resuming. Restoring from park, if applicable. Reset will throw an alarm.
This data field is always present as the first field."
  (let ((st (gensym))
        (st2 (gensym))
        (err (gensym))
        (it (gensym)))
    `(multiple-value-bind (,st2 ,err)
         (ignore-errors
          (let* ((,st (%%stage-2 (format nil "~A" ,s)))
                 (,it (%%value :st ,st)))
            (cd:make-grbl-state :st (if (or (member ,it cd:+grbl-error-states+)
                                            (not (member ,it cd:+grbl-states+)))
                                        :error
                                        ,it)
                                :wpos (%%value :wpos ,st)
                                :bf (%%value :bf ,st)
                                :fs (%%value :fs ,st)
                                :wco (%%value :wco ,st)
                                :ov (%%value :ov ,st)
                                :flags (%%value :flags ,st))))
       (cond (,err
              (env:log->tcp
               (format nil "~%[grbl] parse error '~a' : msg '~a'" ,s ,err))
              (%make-error-grbl-state ,err))
             (t (unless (member (cd:grbl-state-st ,st2) cd:+grbl-open-states+)
                  (env:log->tcp
                   (format nil "~%[grbl] error '~a'" ,s)))
                ,st2)))))

(defmacro %with-crlf (s)
  `(format nil "~a~a~a" ,s #\Return #\Newline))

(defmacro %wrapped-grbl-state (&body forms)
  (let ((st (gensym))
        (err (gensym)))
    `(multiple-value-bind (,st ,err)
         (ignore-errors ,@forms)
       (cond (,err (%make-error-grbl-state ,err))
             (t ,st)))))

(defmacro raw-? ()
  `(%wrapped-grbl-state
     (usl:usb-port-write (%with-crlf "?"))
     (sleep (* 4 env:*grbl-gap-sec*))
     (%parse-grbl-state (usl:usb-port-read))))

(defmacro raw-% ()
  `(%wrapped-grbl-state
     (usl:usb-port-write (%with-crlf "%"))
     (sleep (* 4 env:*grbl-gap-sec*))
     (env:log->tcp (format nil "~%[grbl] reset : '~A'" (usl:usb-port-read)))))

(defun pos-default ()
  (ignore-errors
   (usl:usb-port-write (%with-crlf "G0 X50 Y50 Z-50"))
   (format nil "~%~A" (usl:usb-port-read))))

(defmacro %move (mode &key x y z)
  (let ((cmd (gensym))
        (st (gensym)))
    `(%wrapped-grbl-state
       (let ((,cmd (format nil "~a ~a"
                           (format nil "~a ~a"
                                   (format nil "~a G0 ~a"
                                           (case ,mode
                                             (:g90 "G90")
                                             (:g91 "G91")
                                             (otherwise (error "wrong mode")))
                                           (if ,x (format nil "X~3$" ,x) ""))
                                   (if ,y (format nil " Y~3$" ,y) ""))
                           (if ,z (format nil " Z~3$" ,z) ""))))
         (usl:usb-port-write (%with-crlf ,cmd))
         (env:log->tcp (format nil "~%[grbl] %move : cmd : '~A'" ,cmd)))
       (sleep (* 8 env:*grbl-gap-sec*))
       (let ((,st (raw-?)))
         (while (eq :run (cd:grbl-state-st ,st))
                (env:log->tcp (format nil "~%[grbl] %move : state : '~A'" ,st))
                (sleep (* 4 env:*grbl-gap-sec*))
                (setf ,st (raw-?)))
         ,st))))

(defmacro %move-to   (&key x y z) `(%move :g90 :x ,x :y ,y :z ,z))

(defmacro %move-from (&key x y z)
  ;;(error "%move-from undefined")
  `(%move :g91 :x (if ,(and (numberp `,x) (zerop `,x)) nil ,x)
               :y (if ,(and (numberp `,y) (zerop `,y)) nil ,y)
               :z (if ,(and (numberp `,z) (zerop `,z)) nil ,z)))

(defun homing ()
  (ignore-errors
   (let ((pos (make-list 0)))
     (push (cons :xyz (raw-?)) pos)
     (push (cons :z (%move-to :z 180)) pos)
     (raw-%)
     (%move-to :z -7)
     (push (cons :y (%move-to :y -600)) pos)
     (raw-%)
     (%move-to :y 10)
     (push (cons :x (%move-to :x -800)) pos)
     (raw-%)
     (%move-to :x 10)
     (raw-%)
     (raw-?)
     (%move-to :x 0)
     (raw-%)
     (mapcar (lambda (x) (/ (round x 0.001D0) 1000D0))
             (list
              (- -10 (car (cd:grbl-state-wpos (cdr (assoc :x pos)))))
              (- -10 (cadr (cd:grbl-state-wpos (cdr (assoc :y pos)))))
              (let ((z0 (caddr (cd:grbl-state-wpos (cdr (assoc :xyz pos)))))
                    (z1 (caddr (cd:grbl-state-wpos (cdr (assoc :z pos))))))
                (if (< z0 0)
                    (- z1 z0 7)
                    (- (+ z0 7) z1))))))))

(defun %read-grbl-cmd (cmd fn)
  (let ((it (multiple-value-bind (r err)
                (usl:usb-port-write (%with-crlf cmd))
              (if r
                  (list nil (list r err))
                  (multiple-value-bind (res err)
                      (usl:usb-port-read)
                    (if (stringp res)
                        (funcall fn res)
                        (list nil (list res err))))))))
    (env:log->tcp (format nil "~%[grbl] cmd : ~a result : ~a" cmd it))
    it))

(defun %grbl-state-bf-ok (st)
  (when st
    (let ((bf (cd:grbl-state-bf st)))
      (when bf
        (every #'identity (mapcar #'> bf env:*grbl-bf-thresholds*))))))

(defun %usb-$cmd-io (line)
  (let* ((ln (map 'string #'(lambda (c) (if (standard-char-p c) c #\.)) line))
         (r (%read-grbl-cmd ln 'identity)))
    (if (stringp r)
        (evt:make-usb-msg ln r nil)
        (evt:make-usb-msg ln (first r) (second r)))))

(defun %read-grbl-state ()
  "Return USB reply of GRBL ? status request typed as GRBL-STATE."
  (%read-grbl-cmd "?" '%parse-grbl-state))

(defun %grbl-state! ()
  "Update system-wide GRBL status as valid GRB-?-MSG event or throw GRBL-STATE-UPDATE-ERROR."
  (cd:grbl-state! (%read-grbl-state))
  (handler-case
      (cd:grbl-state)
    (cd:grbl-state-read-error ()
      (signal 'cd:grbl-state-update-error))))

(defun %read-grbl-settings () (%read-grbl-cmd "$$" 'identity))
(defun %read-grbl-parser () (%read-grbl-cmd "$G" 'identity))
(defun %read-grbl-gcode-params () (%read-grbl-cmd "$#" 'identity))
(defun %read-grbl-build-info () (%read-grbl-cmd "$I" 'identity))
(defun %read-grbl-startup-blocks () (%read-grbl-cmd "$N" 'identity))
(defun %read-grbl-check-gcode-mode () (%read-grbl-cmd "$C" 'identity))
(defun %grbl-kill-alarm-lock () (%read-grbl-cmd "$X" 'identity))
(defun %grbl-run-homing-cycle () (%read-grbl-cmd "$H" 'identity))
(defun %grbl-cycle-start () (%read-grbl-cmd "~" 'identity))
(defun %grbl-feed-hold () (%read-grbl-cmd "!" 'identity))
(defun %grbl-reset () (%read-grbl-cmd "%" 'identity))
(defun %grbl-ctl-x ()
  (%read-grbl-cmd (make-string 1 :initial-element #\Can) 'identity))

(defmacro %%maybe-grbl-settings ()
  `(ignore-errors
    (remove-if-not (lambda (x) (and (numberp (car x)) (numberp (cadr x))))
                   (mapcar
                    (lambda (s) (mapcar #'uiop:safe-read-from-string
                                        (str:split #\= (remove #\$ s))))
                    (remove-if-not (lambda (s) (and (position #\$ s)
                                                    (position #\= s)))
                                   (str:split #\Return
                                              (%read-grbl-settings)))))))

(defun %maybe-grbl-settings ()
  (let ((it (%%maybe-grbl-settings)))
    (when (and (consp it) (= env:*grbl-settings-length* (length it))) it)))

(declaim (ftype (function () t) open-fn))
(declaim (ftype (function () t) halt-fn))
(declaim (ftype (function () t) retry-fn))
(declaim (ftype (function () t) alarm-fn))
(declaim (ftype (function () t) error-fn))

(defun %to-tunits-of-state ()
  (let ((st (cd:maybe-grbl-state)))
    (cond  ((member st +grbl-fast-states+)  0)
           ((member st cd:+grbl-retry-states+) env:*grbl-retry-tunits*)
           (t env:*grbl-idle-tunits*))))

(let ((ts-grbl-state (get-internal-real-time)))
  (defun %scheduled-grbl-state! (from-state &optional
                                              (to-tunits (%to-tunits-of-state)))
    (sleep (/ env:*grbl-gap-tunits* internal-time-units-per-second))
    (let ((ts (get-internal-real-time)))
      (when (>= ts (+ ts-grbl-state to-tunits))
        (setf ts-grbl-state (get-internal-real-time))
        (handler-case
            (progn
              (%grbl-state!)
              (let ((st (cd:maybe-grbl-state)))
                (cond ((cd:retry-state? st)
                       (unless (cd:retry-from-state? from-state)
                         (signal 'cd:switch-state :fn #'retry-fn)))
                      ((cd:alarm-state? st)
                       (unless (cd:alarm-from-state? from-state)
                         (signal 'cd:switch-state :fn #'alarm-fn)))
                      ((cd:open-state? st)
                       (unless (cd:open-from-state? from-state)
                         (signal 'cd:switch-state :fn #'open-fn)))
                      ((cd:error-state? st)
                       (unless (cd:error-from-state? from-state)
                         (signal 'cd:switch-state :fn #'error-fn)))
                      (t (env:log->tcp (format nil
                                        "[grbl]  %scheduled-grbl-state! ~s ~A"
                                        "unexpected from-state : "
                                        from-state))
			 (signal 'cd:switch-state :fn #'open-fn)))))
          (cd:grbl-state-update-error ()
            (cond ((cd:error-state? from-state))
                  ((cd:retry-from-state? from-state))
                  ((cd:alarm-from-state? from-state)
                   (signal 'cd:switch-state :fn #'retry-fn))
                  ((cd:open-from-state? from-state)
                   (signal 'cd:switch-state :fn #'retry-fn))
                  (t (env:log->tcp (format nil
                                    "[grbl]  %scheduled-grbl-state! ~s ~A"
                                    "unexpected from-state : "
                                    from-state))
		     (signal 'cd:switch-state :fn #'open-fn)))))))))

(defun %wait-for-bf-ok ()
  (%scheduled-grbl-state! :open 0)
  (until (%grbl-state-bf-ok (cd:maybe-grbl-state))
         (%scheduled-grbl-state! :open env:*grbl-gap-tunits*)))

(let ((t0 (get-internal-real-time)))
  (defun open-fn ()
    (when (> (get-internal-real-time) (+ t0 env:*grbl-idle-tunits*))
      (env:log->tcp (format nil "~%[USB] open-fn"))
      (setf t0 (get-internal-real-time)))
    (awhen (cd:maybe-grbl-state) (when (cd:halt-state? it) (halt-fn)))
    (reset-ts :open)
    (handler-case
        (progn
          (cond ((cd:peek-event :usb-qu-0)
                 (let* ((e (cd:peek!-event :usb-qu-0))
                        (msg (when e (evt:event-msg e))))
                   (case msg
                     (:? (%scheduled-grbl-state! :open 0))
                     (:quit (signal 'cd:quit-task))
                     (:$$ (let ((msg (%read-grbl-settings)))
                            (cd:send-event :ctl-qu-0 (evt:make-event :usb-$$ msg))))
                     (otherwise
                      (env:log->tcp (format nil "unexpected event : ~A" e))))))
                ((cd:peek-event :usb-qu-$)
                 (let* ((e (cd:peek!-event :usb-qu-$))
                        (msg (when e (evt:event-msg e))))
                   (case msg
                     (:? (%scheduled-grbl-state! :open 0))
                     (:$$ (let ((msg (%read-grbl-settings)))
                            (cd:send-event :ctl-qu-$ (evt:make-event :usb-$$ msg))))
                     (:abort
                      (do ((e (cd:peek!-event :usb-qu-ln) (cd:peek!-event :usb-qu-ln)))
                          ((null e))
                       :skip))
                     (:halt (let ((st (cd:maybe-grbl-state)))
                              (when (cd:halt-state? st)
                                (setf (cd:grbl-state-substate st) :halt)
                                (cd:grbl-state! st))
                              (%scheduled-grbl-state! :open 0)))
                     (:resume (let ((st (cd:maybe-grbl-state)))
                                (when (cd:halt-state? st)
                                  (setf (cd:grbl-state-substate st) nil)
                                  (cd:grbl-state! st))
                                (%scheduled-grbl-state! :open 0)))
                     (otherwise
                      (env:log->tcp (format nil "unexpected event : ~A" e))))))
                ((cd:peek-event :usb-qu-ln)
                 (let* ((e (cd:peek-event :usb-qu-ln))
                        (msg (when e (evt:event-msg e))))
                   (cond ((evt:line-msg-p msg)
                          (%wait-for-bf-ok)
                          (cd:peek!-event :usb-qu-ln)
                          (cd:send-event
                           :file-qu-usb
                           (evt:make-event :usb-msg
                                           (%usb-$cmd-io
                                            (evt:line-msg-text msg)))))
                         (t (env:log->tcp
                             (format nil "unexpected event : ~A" e))))))
                (t nil)))
      (cd:grbl-state-read-error ()
        (env:log->tcp "~%[USB] ignore GRBL-STATE-READ-ERROR")
        (open-fn))
      (cd:switch-state (fn)
        (cd:send-event :ctl-qu-0 (evt:make-event :usb-0 :switch))
        (env:log->tcp
         (format nil "~%[USB] new state : ~A ~%~A"
                 (cd:switch-fn fn) (cd:maybe-grbl-state)))
        (funcall (cd:switch-fn fn)))
      (cd:quit-task ()
        (env:log->tcp (format nil "~%-- usb quit ----------------~%"))
        (return-from open-fn)))
    (open-fn)))

(let ((t0 (get-internal-real-time)))
  (defun error-fn ()
    (sleep env:*grbl-error-sec*)
    (env:log->tcp (format nil "~%[USB] error-fn"))
    (let* ((e (cd:peek!-event :usb-qu-0))
           (msg (when e (evt:event-msg e)))
           (t1 (get-internal-real-time)))
      (cond ((eql :? msg)
             (handler-case
                 (%scheduled-grbl-state! :error 0)
               (cd:grbl-state-read-error ()
                 (env:log->tcp "~%[USB] ignore GRBL-STATE-READ-ERROR")
                 (error-fn))
               (cd:switch-state (fn)
                 (cd:send-event :ctl-qu-0 (evt:make-event :usb-0 :switch))
                 (env:log->tcp
                  (format nil "~%[USB] new state : ~A ~%~A"
                          fn (cd:maybe-grbl-state)))
                 (funcall (cd:switch-fn fn)))))
            ((eql :quit msg))
            ((> t1 (+ env:*grbl-retry-tunits* t0))
             (setf t0 t1)
             (if (usl:usb-port-open)
                 (progn
                   (cd:send-event :ctl-qu-0 (evt:make-event :usb-0 :switch))
                   (env:log->tcp
                    (format nil "~%[USB] new state : ~A ~%~A"
                            :error-fn (cd:maybe-grbl-state)))
                   (open-fn))
                 (error-fn)))
            (t (error-fn))))))

(let ((curr-retries 0))
  (defun retry-fn ()
    (env:log->tcp (format nil "~%[USB] retry-fn ~A" curr-retries))
    (reset-ts :retry)
    (incf curr-retries)
    (cond ((> curr-retries env:*grbl-retries*)
           (setf curr-retries 0)
           (error-fn))
          (t (handler-case
                 (%scheduled-grbl-state! :retry)
               (cd:grbl-state-read-error ()
                 (env:log->tcp "~%[USB] ignore GRBL-STATE-READ-ERROR")
                 (retry-fn))
               (cd:switch-state (fn)
                 (cd:send-event :ctl-qu-0 (evt:make-event :usb-0 :switch))
                 (env:log->tcp
                  (format nil "~%[USB] new state : ~A ~%~A"
                          fn (cd:maybe-grbl-state)))
                 (funcall (cd:switch-fn fn))))
             (sleep (/ env:*grbl-retry-tunits* internal-time-units-per-second))
             (retry-fn)))))

(defun alarm-fn () (error-fn))

(defun usb-fn ()
  (env:log->tcp (format nil "~%-- usb open ----------------~%"))
  (handler-case
      (let* ((err (usl:usb-port-open))
             (err-str (format nil "open port error : ~A" err)))
        (cond (err
               (reset-ts :error)
               (cd:send-event cd:ctl-qu-0
                              (evt:make-event :usb-err err-str))
               (retry-fn))
              (t (open-fn))))
    (cd:grbl-state-read-error ()
      (env:log->tcp "~%[USB] ignore GRBL-STATE-READ-ERROR")
      (retry-fn))
    (cd:switch-state (fn)
      (cd:send-event :ctl-qu-0 (evt:make-event :usb-0 :switch))
      (env:log->tcp
       (format nil "~%[USB] new state : ~A ~%~A" fn (cd:maybe-grbl-state)))
      (funcall (cd:switch-fn fn)))
    (cd:quit-task ()
      (env:log->tcp (format nil "~%-- usb quit ----------------~%"))
      (return-from usb-fn)))
  (loop 'forever))


#||#

