
(in-package #:cnc-host/repl)

#|
(x y z) = (a1 a 2 a3) + l1 (b1 b2 b3)

(x y z) = l2 ((x0 - c1) (y0 - c2)  (z0 - c3))

(a1 a 2 a3) + l1 (b1 b2 b3) = l2 ((x0 - c1) (y0 - c2)  (z0 - c3))

M = [ b c-x0 a ] (l1 l2 -1) = (0 0 0)

a = (rot phi theta zeta) (1 0 0)
b = (rot phi theta zeta) (0 1 0)

|#


(defun minor (m col)
  (loop with dim = (1- (array-dimension m 0))
        with result = (make-array (list dim dim))
        for i from 1 to dim
        for r = (1- i)
        do (loop with c = 0
                 for j to dim
                 when (/= j col)
                   do (setf (aref result r c) (aref m i j))
                      (incf c))
        finally (return result)))
 
(defun det (m)
  (assert (= (array-rank m) 2))
  (assert (= (array-dimension m 0) (array-dimension m 1)))
  (let ((dim (array-dimension m 0)))
    (if (= dim 1)
        (aref m 0 0)
        (loop for col below dim
              for sign = 1 then (- sign)
              sum (* sign (aref m 0 col) (det (minor m col)))))))
 
(defun replace-column (m col values)
  (let* ((dim (array-dimension m 0))
         (result (make-array (list dim dim))))
    (dotimes (r dim result)
      (dotimes (c dim)
        (setf (aref result r c)
              (if (= c col) (aref values r) (aref m r c)))))))
 
(defun solve (m v)
  (loop with dim = (array-dimension m 0)
        with det = (det m)
        for col below dim
        collect (/ (det (replace-column m col v)) det)))
 
(solve #2A((2 -1  5  1)
           (3  2  2 -6)
           (1  3  3 -1)
           (5 -2 -3  3))
       #(-3 -32 -47 49))

(defun rot (phi theta zeta)
  (let ((rot (make-array '(3 3)))
        (sin_p (sin phi))
        (cos_p (cos phi))
        (sin_t (sin theta))
        (cos_t (cos theta))
        (sin_z (sin zeta))
        (cos_z (cos zeta)))
    ;; yaw z
    ;; pitch t
    ;; roll p
    (setf (aref rot 0 0) (* cos_z cos_t))
    (setf (aref rot 0 1) (- (* cos_z sin_t sin_p) (* sin_z cos_p)))
    (setf (aref rot 0 2) (+ (* cos_z sin_t cos_p) (* sin_z sin_p)))
    (setf (aref rot 1 0) (* sin_z cos_t))
    (setf (aref rot 1 1) (+ (* sin_z sin_t sin_p) (* cos_z cos_p)))
    (setf (aref rot 1 2) (- (* sin_z sin_t cos_p) (* cos_z sin_p)))
    (setf (aref rot 2 0) (* -1 sin_t))
    (setf (aref rot 2 1) (* cos_t  sin_p))
    (setf (aref rot 2 2) (* cos_t cos_t))
    rot))

#|

a = (rot phi theta zeta) (1 0 0)
b = (rot phi theta zeta) (0 1 0)
<a,a> = <b,b> = 1
<a,b> = <b,a> = 0

(xr-xr xr-y xr-z) = (a-x a-y a-z) + l1 (b-x b-y b-z)

<xr,b> = l1
<xr,a> = 1

(xr-x xr-y xr-z) = l2 ((xr0-x - cr-x) (xr0-y - cr-y) (xr0-z - cr-z))

<xr,b> = l2 <xr0-cr,b>
<xr,a> = l2 <xr0-cr,a>

l2 = 1 / <xr0-cr,a>
l1 = l2 <xr0-cr,b> 

|#

(defun mult (m x y z)
  (declare (number x y z))
  (list (+ (* (aref m 0 0) x) (* (aref m 0 1) y) (* (aref m 0 2) z))
        (+ (* (aref m 1 0) x) (* (aref m 1 1) y) (* (aref m 1 2) z))
        (+ (* (aref m 2 0) x) (* (aref m 2 1) y) (* (aref m 2 2) z))))

(defun euklid-2 (x y)
  (declare (list x y))
  (+ (* (nth 0 x) (nth 0 y)) (* (nth 1 x) (nth 1 y)) (* (nth 2 x) (nth 2 y))))

(defun make-projection-fn (phi theta zeta c-x c-y c-z)
  (declare (number phi theta zeta c-x c-y c-z))
  (let* ((r (rot phi theta zeta))
         (cr (mult r c-x c-y c-z))
         (cr-x (nth 0 cr))
         (cr-y (nth 1 cr))
         (cr-z (nth 2 cr))
         (e-x (list (aref r 0 0) (aref r 1 0) (aref r 2 0)))
         (e-y (list (aref r 0 1) (aref r 1 1) (aref r 2 1))))
    (print r)
    (lambda (x y z)
      (declare (number x y z))
      (let* ((xr0-cr (mult r (- x cr-x) (- y cr-y) (- z cr-z)))
             (l2 (/ 1 (euklid-2 xr0-cr e-x)))
             (l1 (* l2 (euklid-2 xr0-cr e-y))))
        ;;(print xr0-cr)
        (list l1 l2)))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun make-projection-fn2 (phi theta zeta c-x c-y c-z)
  (lambda (x y z)
    (let* ((m (make-array '(3 3)))
           (r (rot phi theta zeta))
           (e-x (solve r #(1 0 0)))
           (e-y (solve r #(0 1 0)))) 
      (setf (aref m 0 0) (nth 0 e-y))
      (setf (aref m 0 1) (- c-x x))
      (setf (aref m 0 2) (nth 0 e-x))
      (setf (aref m 1 0) (nth 1 e-y))
      (setf (aref m 1 1) (- c-y y))
      (setf (aref m 1 2) (nth 1 e-x))
      (setf (aref m 2 0) (nth 2 e-y))
      (setf (aref m 2 1) (- c-z z))
      (setf (aref m 2 2) (nth 2 e-x))
      (solve m #(0 0 0)))))


