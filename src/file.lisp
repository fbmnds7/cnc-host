
(in-package #:cnc-host/file)


(defmacro aif (test-form then-form &optional else-form)
  `(let ((it ,test-form)) (if it ,then-form ,else-form)))

(defmacro aand (&rest args)
  "Hint: (aand (owner x) (address it) (town it))"
  (cond ((null args) t)
        ((null (cdr args)) (car args))
        (t `(aif ,(car args) (aand ,@(cdr args))))))

(defmacro awhen (test-form &body body)
  `(aif ,test-form (progn ,@body)))

(defmacro while (test &rest body) `(do () ((not ,test)) ,@body))

(defvar *file-fn* nil)
(defvar *sub-st* nil)
(defvar *gc-state* nil) ; -> /= grbl-state TODO make it a closure init-st-acc 
(defvar *st-acc* nil)
(defvar *nc-path* nil)
(defvar *db-path* nil)
(defvar *sha256* nil)
(defvar *file-content* nil)
(defvar *file-length* nil)
(defvar *line-nr* 0)
(defvar *line-on-abort* nil)
(defvar *line-on-error* nil)
(defvar *line-on-halt* nil)

(defvar *timestamp-start* nil)
(defvar *timestamp-send-line* nil)
(defvar *timestamp-end* (get-internal-real-time))

(defun state ()
  (list *file-fn* *sub-st*
        *gc-state* *st-acc*
        *nc-path* *db-path* *sha256*
        *file-content* *file-length*
        *line-nr* *st-acc*
        *line-on-abort* *line-on-error* *line-on-halt*
        *timestamp-start* *timestamp-send-line* *timestamp-end*))

(defun reset-file ()
  (setf *st-acc* nil)
  (setf *nc-path* nil)
  (setf *db-path* nil)
  (setf *sha256* nil)
  (setf *file-content* nil)
  (setf *file-length* nil)
  (setf *line-nr* 0)
  (setf *line-on-abort* nil)
  (setf *line-on-error* nil)
  (setf *line-on-halt* nil))

(defun reset-ts ()
  (setf *timestamp-start* nil)
  (setf *timestamp-send-line* nil)
  (setf *timestamp-end* (get-internal-real-time)))

(defun proj-fn (x y z) (list (+ x (* y (cos (/ pi 3))))
                             (+ z (* y (sin (/ pi 3))))))

(defun  ws-send-woc-pos (p)
  (when (eql 'gc:point-3 (type-of p))
    (with-accessors ((x x) (y y) (z z)) p
      (let ((p-xy (proj-fn (or (gc:x p) 0)
                           (or (gc:y p) 0)
                           (or (gc:z p) 0))))
        (ws:ws-send (or (first p-xy) 0)
                    (or (second p-xy) 0))))))

(defun init-st-acc ()
  (let ((st-acc-0 (make-instance 'gc:stage-acc
                                 :gc-state-0 (or *gc-state*
                                                 (make-instance 'gc:gc-state
                                                                :mode :abs)))))
    (unless (gc:woc-pos (gc:gc-state-0 st-acc-0))
      (setf (gc:woc-pos (gc:gc-state-0 st-acc-0))
            (make-instance 'gc:point-3 :x 0 :y 0 :z 0)))
    (setf *st-acc* st-acc-0)))

(defun %unexpected-event (fn e)
  (format nil "[cnc-host/file] ~A : unexpected event : ~A" fn e))

(defun %not-yet-defined-event (fn e)
  (format nil "[cnc-host/file] ~A : not yet defined event : ~A" fn e))

(defun %grbl-state-check (st grp)
  (when (cd:grbl-state-p st)
    (member (cd:grbl-state-st st) grp)))

(defun %grbl-state-ok ()
  (let ((st (cd:maybe-grbl-state)))
    (when (%grbl-state-check st cd:+grbl-open-states+)
      (awhen (cd:grbl-state-bf st)
        (every #'identity (mapcar #'> it env:*grbl-bf-thresholds*))))))

(defun %grbl-state-open ()
  (%grbl-state-check (cd:maybe-grbl-state) cd:+grbl-open-states+))

(defun %grbl-state-retry ()
  (%grbl-state-check (cd:maybe-grbl-state) cd:+grbl-retry-states+))

(defun %grbl-state-alarm ()
  (%grbl-state-check (cd:maybe-grbl-state) cd:+grbl-alarm-states+))

(defun %grbl-state-error ()
  (%grbl-state-check (cd:maybe-grbl-state) cd:+grbl-error-states+))

(defun %prepare-nc-file-processing (nc-path)
  (multiple-value-bind (nc err)
      (ignore-errors
       (with-open-file (s nc-path)
         (loop for ln = (read-line s nil nil nil)
               while ln
               collect (§:standard-str ln))))
    (cond (nc (setf *nc-path* nc-path)
              (setf *file-content* nc)
              (setf *file-length* (length nc))
              (setf *line-nr* 0)
              (return-from %prepare-nc-file-processing :job))
          (t (cd:send-event :ctl-qu-$
                            (evt:make-event :file-$ (format nil "~A" err)))
             (return-from %prepare-nc-file-processing :idle)))))

(declaim (ftype (function () t) halt-fn))
(declaim (ftype (function () t) job-fn))
(declaim (ftype (function () t) idle-fn))
(declaim (ftype (function () t) open-fn))
(declaim (ftype (function () t) retry-fn))
(declaim (ftype (function () t) alarm-fn))
(declaim (ftype (function () t) error-fn))  

(define-condition unexpected-event (error)
  ((unexpected-e  :initarg :e  :reader unexpected-e)
   (unexpected-fn :initarg :fn :reader unexpected-fn)
   (unexpected-f2 :initarg :f2 :reader unexpected-f2)))
(define-condition not-yet-defined-event (error)
  ((not-yet-defined-e  :initarg :e  :reader not-yet-defined-e)
   (not-yet-defined-fn :initarg :fn :reader not-yet-defined-fn)
   (not-yet-defined-f2 :initarg :f2 :reader not-yet-defined-f2)))

(defun wrap-fn (fn)
  (handler-case
      (funcall fn)
    (cd:switch-state (st)
      (env:log->tcp (format nil "~%[FILE] new state : ~A" st))
      (cd:send-event :ctl-qu-0 (evt:make-event :file-0 :switch))
      (awhen (cd:switch-sub-st st) (setf *sub-st* it))
      (funcall (cd:switch-fn st)))
    (unexpected-event (s)
      (%unexpected-event (unexpected-fn s)
                         (unexpected-e  s))
      (funcall (unexpected-f2 s)))
    (not-yet-defined-event (s)
      (%not-yet-defined-event (not-yet-defined-fn s)
                              (not-yet-defined-e  s))
      (funcall (not-yet-defined-f2 s)))))

(defun %switch-on-maybe-grbl-state (st from-state)
  (wrap-fn
   (lambda ()
     (cond ((not st)
            (signal 'cd:switch-state :fn #'open-fn :sub-st :idle))
           ((cd:retry-state? st)
            (unless (cd:retry-from-state? from-state)
              (signal 'cd:switch-state :fn #'retry-fn)))
           ((cd:alarm-state? st)
            (unless (cd:alarm-from-state? from-state)
              (signal 'cd:switch-state :fn #'alarm-fn)))
           ((cd:open-state? st)
            (unless (cd:open-from-state? from-state)
              (signal 'cd:switch-state :fn #'open-fn :sub-st from-state)))
           ((cd:error-state? st)
            (unless (cd:error-from-state? from-state)
              (signal 'cd:switch-state :fn #'error-fn)))
           (t (error (format nil
                             "[grbl]  %scheduled-grbl-state! ~s ~A"
                             "unexpected from-state : "
                             from-state)))))))

(defun error-fn ()
  (setf *file-fn* :error-fn)
  (sleep env:*grbl-error-sec*)
  (when (evt:event? (cd:peek!-event :file-qu-0) :quit)
    (signal 'cd:quit-task)))

(defun alarm-fn ()
  (setf *file-fn* :alarm-fn)
  (sleep env:*grbl-error-sec*)
  (when (evt:event? (cd:peek!-event :file-qu-0) :quit)
    (signal 'cd:quit-task)))

(defun job-fn ()
  (setf *file-fn* :job-fn)
  (setf *sub-st*  :job)
  (if (cd:peek-event :file-qu-0)
      (when (evt:event? (cd:peek!-event :file-qu-0) :quit)
        (signal 'cd:quit-task))
      (if (cd:peek-event :file-qu-$)
          (let* ((e (cd:peek!-event :file-qu-$))
                 (msg (when e (evt:event-msg e))))
            (cond ((eql :abort msg)
                   (reset-file)
                   (reset-ts)
                   (setf *sub-st* :idle))
                  ((eql :halt msg)
                   (setf *sub-st* :halt))
                  ;; do not accept line-msg -> ?
                  ;; do not accept :resume  -> halt-fn
                  (t (env:log->tcp (%unexpected-event :%job-qu-$-fn e)))))
          (progn
            (cd:peek!-event :file-qu-usb)         ; ignore USB messages
            (while (and *file-content*
                        (numberp *line-nr*)
                        (numberp *file-length*)
                        (%grbl-state-ok)
                        (not (cd:full-qu-p :usb-qu-ln))) 
                   (if (< *line-nr* *file-length*)
                       (let ((ln (evt:make-line-msg (1+ *line-nr*)
                                                    (nth *line-nr*
                                                         *file-content*))))
                         (cd:send-event :usb-qu-ln (evt:make-event :file-ln ln))
                         (incf *line-nr*))
                       (let ((done (evt:make-event :file-$ :done)))
                         (cd:send-event :ctl-qu-$ done)
                         (reset-file)
                         (reset-ts)
                         (setf *sub-st* :idle))))))))

(defun halt-fn ()
  (setf *file-fn* :halt-fn)
  (setf *sub-st*  :halt)
  (if (cd:peek-event :file-qu-0)
      (when (evt:event? (cd:peek!-event :file-qu-0) :quit)
        (signal 'cd:quit-task))
      (let* ((e (cd:peek!-event :file-qu-$))
             (msg (evt:event-msg e)))
        (cond ((eql :resume msg)
               (setf *sub-st* :job))
              ;; do not accept line-msg -> ?
              ;; do not accept :halt    -> halt-fn 
              (t (env:log->tcp (%unexpected-event :%halt-qu-$-fn e)))))))

(defun idle-fn ()
  (setf *file-fn* :idle-fn)
  (setf *sub-st*  :idle)
  (cond ((cd:peek-event :file-qu-0)
         (when (evt:event? (cd:peek!-event :file-qu-0) :quit)
           (signal 'cd:quit-task)))
        ((cd:peek-event :file-qu-$)
         (let* ((e (cd:peek!-event :file-qu-$))
                (msg (evt:event-msg e)))
           (cond ((and (evt:nc-file-msg-p msg)
                       (eql :nc (evt:nc-file-msg-type msg)))
                  (setf *sub-st* (%prepare-nc-file-processing
                                  (evt:nc-file-msg-path msg))))
                 ((and (evt:nc-file-msg-p msg)
                       (eql :db (evt:nc-file-msg-type msg)))
                  (env:log->tcp (%not-yet-defined-event :idle-fn e)))
                 (t
                  (env:log->tcp (%unexpected-event :idle-fn e))))))
        (t nil)))

(defun open-fn ()
  (setf *file-fn* :open-fn)
  (case *sub-st*
    (:idle (idle-fn))
    (:job (job-fn))
    (:halt (halt-fn))
    (otherwise (error "unexpected GRBL open substate"))))

(defun file-fn ()
  (env:log->tcp (format nil "~%-- file idle ---------------~%"))
  (reset-file)
  (reset-ts)
  (setf *sub-st* :idle)
  (handler-case
      (loop 'forever
            (let ((st (cd:maybe-grbl-state)))
              (cond ((cd:retry-state? st)
                     (sleep (/ env:*grbl-retry-tunits*
                               internal-time-units-per-second)))
                    ((cd:error-state? st) (error-fn))
                    ((cd:open-state? st)  (open-fn))
                    ((cd:alarm-state? st) (alarm-fn))
                    (t nil))))
    (cd:quit-task ()
      (reset-file)
      (reset-ts)
      (env:log->tcp (format nil "~%-- file quit ---------------~%")))))

#|
|#


