;;;; cnc-host.lisp

(in-package #:cnc-host)

;;(defparameter *repl* (make-instance 'repl :state :init))

;;(defparameter *usb-sp* (make-instance 'usb-sp :state :init))
;;(setf cnc-host/usb-sp::*usb-sp* (make-instance 'usb-sp :state :init))

(defclass cnc-state ()
  ((settings
    :initarg :settings
    :initform *settings*
    :accessor settings)
   (machine-coordinate
    :initarg :machine-coordinate
    :initform '(:x 0.0 :y 0.0 :z 0.0)
    :accessor machine-coordinate)
   (work-coordinate
    :initarg :work-coordinate
    :initform '(:x 0.0 :y 0.0 :z 0.0)
    :accessor work-coordinate)
   (cnc-file
    :accessor cnc-file)))

;;(defparameter *repl-controller-active* nil)

(defun main ()
#|
  (cnc-host/repl:run
   *repl*
   (make-task :repl-dispatcher (cnc-host/repl:dispatch-fn *repl*))
   (make-task :repl-controller (cnc-host/repl:control-fn  *repl*))
   (make-task :repl-processor  (cnc-host/repl:process-fn  *repl*))
   cnc-host/repl:*controller-active*)
|#
#|
  (cnc-host/usb-sp:run
   cnc-host/usb-sp::*usb-sp*
   (make-task :usb-sp-dispatcher (cnc-host/usb-sp::dispatch-fn cnc-host/usb-sp::*usb-sp*))
   (make-task :usb-sp-controller (cnc-host/usb-sp::control-fn  cnc-host/usb-sp::*usb-sp*))
   (make-task :usb-sp-processor  (cnc-host/usb-sp::process-fn  cnc-host/usb-sp::*usb-sp*)))
|# 
 ;;(start-repl-tcp *repl* *repl-port*)
  )

;;(main)

