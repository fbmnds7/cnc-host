
(in-package #:cnc-host/task)

(defstruct (task
            (:constructor %make-task (&key tag fn)))
  tag fn)

(defun make-task (tag fn) (%make-task :tag tag :fn fn))

(defun run (task channel)
  (let ((lp:*task-category* (task-tag task)))
    (lp:submit-task channel (task-fn task))))

