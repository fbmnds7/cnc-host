
(in-package #:cnc-host/ws-feed)


(defvar *server*   nil)
(defvar *services* nil)
(defvar *feed*     nil)

(defclass service (hs:websocket-resource)
  ((name
    :initarg :name
    :initform (error "Name this service!")
    :reader name))
  (:default-initargs :client-class 'client))

(defclass client (hs:websocket-client)
  ((name
    :initarg :user-agent
    :initform (error "Name this client!")
    :reader name)))

(defun find-service (request)
  (find (ht:script-name request) *services* :test #'string= :key #'name))

(defun broadcast (service message &rest args)     ;
  (loop for peer in (hs:clients service) ;
     do (hs:send-text-message peer (apply #'format nil message args)))) ;

(defmethod hs:client-connected ((srv service) client)
  (broadcast srv "~a has joined ~a" (name client) (name srv)))

(defmethod hs:client-disconnected ((srv service) client)
  (broadcast srv "~a has left ~a" (name client) (name srv)))

(defmethod hs:text-message-received ((srv service) client message)
  (broadcast srv "~a says ~a" (name client) message))  

(defun run-server (port)
  (pushnew 'find-service hs:*websocket-dispatch-table*)
  (unless *services*
    (setf *services* (list (make-instance 'service :name "/feed")
                           (make-instance 'service :name "/undefined"))))
  (unless *feed*
    (setf *feed* (first *services*)))
  (unless *server*
    (setf *server* (make-instance 'hs:websocket-acceptor :port port))
    (ht:start *server*)))

(defmethod run-feed-server ((feed string) (port integer))
  (declare (ignore feed))
  (run-server port))

(defmethod ws-send ((x number) (y number))
  (broadcast *feed* (format nil "{\"x\":~,4f,\"y\":~,4f}" x y))  
  (sleep 1))


