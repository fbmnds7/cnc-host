
(in-package #:cnc-host/event)

(defmacro aif (test-form then-form &optional else-form)
  `(let ((it ,test-form)) (if it ,then-form ,else-form)))

(defstruct (event
            (:constructor %%make-event (&key tag ts msg))
            (:constructor %make-event (&key tag msg))
            (:print-function
             (lambda (e stream depth)
               (declare (ignore depth))
               (format stream
                       (if (event-msg e)
                           (format nil "~A : ~A" (event-tag e) (event-msg e))
                           (format nil "~A" (event-tag e))))))
            :predicate)
  tag (ts (get-internal-real-time)) msg)

(defun make-event (tag &optional (msg nil))
  (%make-event :tag tag :msg msg))

(defun %event-of-tag? (e tag) (when (and (event-p e) (eql tag (event-tag e))) e))

(defun event? (o &optional (tag nil))
  (if tag
      (%event-of-tag? o tag)
      (when (event-p o) o)))

(defstruct (woc-xy-msg
            (:constructor %make-woc-xy-msg (&key x y))
            (:constructor make-woc-xy-msg (x y))
            (:print-function
             (lambda (msg stream depth)
               (declare (ignore depth))
               (format stream
                       (format nil "x = ~d : y = ~d"
                               (woc-xy-msg-x msg)
                               (woc-xy-msg-y msg)))))
            :predicate)
  x y)

(defstruct (line-msg
            (:constructor %make-line-msg (&key nr text err))
            (:constructor make-line-msg (nr text &optional (err nil)))
            (:print-function
             (lambda (msg stream depth)
               (declare (ignore depth))
               (format stream
                       (aif (line-msg-err msg)
                            (format nil "~d : ~s : ~s"
                                    (line-msg-nr msg)
                                    (line-msg-text msg)
                                    it)
                            (format nil "~d : ~s"
                                    (line-msg-nr msg)
                                    (line-msg-text msg))))))
            :predicate)
  nr text err)

(defmethod make-line-msg-event (nr (ln string))
  (make-event :file-ln (make-line-msg nr ln)))

(defun line-msg-event? (e)
  (when (and (event? e :file-ln)
             (line-msg-p (event-msg e)))
    e))

(defstruct (usb-msg
            (:constructor %make-usb-msg (&key cmd res err))
            (:constructor make-usb-msg (cmd res &optional (err nil)))
            (:print-function
             (lambda (msg stream depth)
               (declare (ignore depth))
               (format stream
                       (aif (usb-msg-err msg)
                            (format nil "~s : ~A : ~A"
                                    (usb-msg-cmd msg)
                                    (usb-msg-res msg)
                                    it)
                            (format nil "~s : ~A"
                                    (usb-msg-cmd msg)
                                    (usb-msg-res msg))))))
            :predicate)
  cmd res err)

(defstruct (grbl-$$-msg
            (:constructor make-grbl-$$-msg (&key params))
            :predicate)
  params)

(defstruct (nc-file-msg
            (:constructor make-nc-file-msg (&key type path))
            :predicate)
  type path)

(defmacro %make-predicates (l)
  (let ((?s (mapcar #'(lambda (s) (§:symb s "?")) l))
        (ps (mapcar #'(lambda (s) (§:reread s "-p")) l)))
    `(progn
       ,@(mapcar #'(lambda (x y) `(defun ,x (o) (when (,y o) o)))
                 `,?s `,ps))))

(%make-predicates
 (grbl-$$-msg woc-xy-msg line-msg usb-msg nc-file-msg))


