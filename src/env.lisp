
(in-package #:cnc-host/env)


(defvar *N* 12)
;;(defvar *task-priority* :default)

(defvar *log-host* "127.0.0.1")
(defvar *log-port* 8081)

(defun log->tcp (e)
  (multiple-value-bind (r err)
      (ignore-errors
       (us:with-client-socket (socket stream *log-host* *log-port*)
         (format stream "~A" e)
         (force-output stream)))
    (when err r (format t "~A" e))))

(defvar *repl-host* "127.0.0.1")
(defvar *repl-port* 5001)

(defvar *ws-client-url* "ws://127.0.0.1:5000")

(defvar *usb-sp-port* "/dev/ttyACM0")
(defvar *usb-sp-qu-ln-capacity* 100)
(defvar *usb-sp-timeout-tunits* (* 60 internal-time-units-per-second))

(defvar *grbl-settings-length* 65)

(defvar *grbl-retries* 5)
(defvar *grbl-bf-thresholds* '(25 750))
(defvar *grbl-gap-sec*      0.05)
(defvar *grbl-gap-tunits*   (* *grbl-gap-sec* internal-time-units-per-second))
(defvar *grbl-idle-tunits*  (* 60             internal-time-units-per-second))
(defvar *grbl-retry-tunits* (*  5             internal-time-units-per-second))
(defvar *grbl-error-sec*    1)

;(do-external-symbols (sy :cnc-host/env) (assert sy))




