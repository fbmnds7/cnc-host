(in-package #:cnc-host/repl)


(defun parse-repl-e (e pre)
  (let ((text (string-trim " " (replp e))))
    (if text
        (or (cond ((string= ":q" text)
                   ;(push-event *ctl-$-queue* (make-event :exit))
                   )
                  ((string= ":a" text)
                   (format t "~%read :a")
                   ;(push-event *ctl-0-queue* (make-event :alarm))
                   )
                  ((string= ":spr+" text)
                   ;(push-event *ctl-$-queue* (make-event :usb-sp-read-prio))
                   )
                  ((starts-with? ":spw+ " text)
                   #|
                   (push-event *ctl-$-queue*
                               (make-text-event
                                :usb-sp-write-prio
                                (substring (length ":spw+ ") nil text)))
                   |#                 
                   )
                  ((starts-with? ":spw " text)
                   #|
                   (push-event *ctl-$-queue*
                               (make-text-event
                                :usb-sp-write
                                (substring (length ":spw ") nil text)))
                   |#
                   )
                  ((starts-with? ":f2" text)
                   (setf (unit *gc-state*) :mm)
                   (setf (mode *gc-state*) :abs)
                   (process-gc-file "~/projects/cnc-host/nc/leg.nc"))
                  ((starts-with? ":f" text)
                   (setf (unit *gc-state*) :mm)
                   (setf (mode *gc-state*) :abs)
                   ;(process-gc-file "~/projects/cnc-host/nc/cubic.nc")
                   )
                  (t
                   #|
                   (log->tcp2 (make-log-event :info
                                                (format nil "~A gcode ~A"
                                                        pre 'text)))
                     (log->tcp2 (make-log-event :info
                                                (format-by-slots
                                                 (stage-1 (make-stage-acc text))
                                                 stage-acc-slots)))
                   |#
                   nil
                     ))
            t) 
        nil)))

