(in-package #:cnc-host/repl-rl)

(defvar *cmds*  '("eat" "get" "throw" "quit"))
(defvar *subcmds* '("banana" "apple" "orange" "banana_two"))

(defun custom-complete (text start end)
  "text is the partially entered word.
   start and end are its position in `*line-buffer*'.
   See cl-readline's documentation.
   "
  (declare (ignore end))
  (labels ((select-completions (list)
             (let ((els (remove-if-not (lambda (it)
                                         (str:starts-with? text it))
                                       list)))
               (if (cdr els)
                   (cons (str:prefix els) els)
                   els))))
    (if (zerop start)
        (select-completions *cmds*)
        (select-completions *subcmds*))))

(defun ctrl-o (arg key)
  (declare (ignore arg key))
  (rl:insert-text "ctrl-o insert dummy text"))

(defun updatep (x y)
  (string/= (string-trim " " x)
            (string-trim " " y)))

(defun run-rl (fn i)
  (rl:register-function :complete #'custom-complete)
  (rl:bind-keyseq "\\C-o" #'ctrl-o)
  (handler-case
      (let ((text (rl:readline
		   :prompt (format nil "cnc [~a]> " i)
                   :add-history t
		   :novelty-check #'updatep)))
	(funcall fn text "cnc-host/repl-rl:run-rl"))
    (#+sbcl sb-sys:interactive-interrupt
     #+ccl  ccl:interrupt-signal-condition
     #+clisp system::simple-interrupt-condition
     #+ecl ext:interactive-interrupt
     #+allegro excl:interrupt-signal
     () (progn
          ;; (format *error-output* "Aborting.~&")
          (uiop:quit)))
    (error (c) (format t "Woops, an unknown error occured:~&~a~&" c))))

