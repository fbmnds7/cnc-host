
(in-package #:cnc-host/control-data)


(setf lp:*kernel* (lp:make-kernel env:*N*))

(define-condition switch-state (error)
  ((fn     :initarg :fn     :reader switch-fn)
   (sub-st :initarg :sub-st :reader switch-sub-st :initform nil)))
(define-condition quit-task (error) ())
(define-condition grbl-state-update-error (error) ())
(define-condition grbl-state-read-error (error) ())

(defvar +grbl-open-states+ '(:open :idle :run :jog :sleep :halt))
(defvar +grbl-retry-states+ '(:check :home))
(defvar +grbl-alarm-states+ '(:alarm :hold0 :hold1
                              :door0 :door1 :door2 :door3))
(defvar +grbl-error-states+ '(:error :nil :|| nil))
(defvar +grbl-states+ (append +grbl-open-states+
                              +grbl-retry-states+
                              +grbl-alarm-states+
                              +grbl-error-states+))

(defstruct (grbl-state
            (:constructor
                make-grbl-state (&key st wpos bf fs (wco nil) (ov nil) flags
                                      (substate nil) (err-msg nil)))
            :predicate)
  st wpos bf fs wco ov flags (ts (get-universal-time)) substate err-msg)

(defun error-from-state? (f-st) (member f-st +grbl-error-states+))
(defun alarm-from-state? (f-st) (member f-st +grbl-alarm-states+))
(defun retry-from-state? (f-st) (member f-st +grbl-retry-states+))
(defun open-from-state?  (f-st) (member f-st +grbl-open-states+))

(defun error-state? (st)
  (when (and (grbl-state-p st)
             (member (grbl-state-st st) +grbl-error-states+))
    st))

(defun alarm-state? (st)
  (when (and (grbl-state-p st)
             (member (grbl-state-st st) +grbl-alarm-states+))
    st))

(defun retry-state? (st)
  (when (and (grbl-state-p st)
             (member (grbl-state-st st) +grbl-retry-states+))
    st))

(defun open-state? (st)
  (when (and (grbl-state-p st)
             (member (grbl-state-st st) +grbl-open-states+))
    st))

(defun halt-state? (st)
  (when (and (grbl-state-p st)
             (eql :halt (grbl-state-substate st)))
    st))

(defun switch-on-maybe-grbl-state (open-fn retry-fn alarm-fn error-fn)
  (lambda (st from-state)
    (cond ((retry-state? st)
           (unless (retry-from-state? from-state)
             (signal 'switch-state :fn retry-fn)))
          ((alarm-state? st)
           (unless (alarm-from-state? from-state)
             (signal 'switch-state :fn alarm-fn)))
          ((open-state? st)
           (unless (open-from-state? from-state)
             (signal 'switch-state :fn open-fn)))
          ((error-state? st)
           (unless (error-from-state? from-state)
             (signal 'switch-state :fn error-fn)))
          (t (error (format nil
                            "[grbl]  %scheduled-grbl-state! ~s ~A"
                            "unexpected from-state : "
                            from-state))))))

(defun make-observable (chan) (cq:make-cqueue chan 1))

(defun observe (cq) (cq:cqpeek cq))

(defmethod update-observable (cq (st grbl-state))
  (cq:cqpeek! cq)
  (cq:cqpush cq st))

(defstruct (ctl-chan
            (:constructor %make-ctl-chan (&key ch qu-0 qu-$ qu-job qu-usb))
            :predicate)
  ch qu-0 qu-$ qu-job qu-usb)

(defstruct (file-chan
            (:constructor %make-file-chan (&key ch qu-0 qu-$ qu-usb))
            :predicate)
  ch qu-0 qu-$ qu-usb)

(defstruct (usb-chan
            (:constructor %make-usb-chan (&key ch qu-0 qu-? qu-$ qu-ln))
            :predicate)
  ch qu-0 qu-? qu-$ qu-ln)

(defstruct (repl-chan
            (:constructor %make-repl-chan (&key ch qu-0 qu-msg))
            :predicate)
  ch qu-0 qu-msg)

(defun make-ctl-chan ()
  (let ((ctl-ch (lp:make-channel)))
    (%make-ctl-chan
     :ch     ctl-ch
     :qu-0   (cq:make-cqueue ctl-ch)
     :qu-$   (cq:make-cqueue ctl-ch)
     :qu-usb (cq:make-cqueue ctl-ch))))

(defun make-file-chan ()
  (let ((file-ch (lp:make-channel)))
    (%make-file-chan
     :ch     file-ch
     :qu-0   (cq:make-cqueue file-ch)
     :qu-$   (make-observable file-ch)
     :qu-usb (cq:make-cqueue file-ch))))

(defun make-usb-chan ()
  (let ((usb-ch (lp:make-channel)))
    (%make-usb-chan
     :ch    usb-ch
     :qu-0  (cq:make-cqueue usb-ch)
     :qu-?  (cq:make-cqueue usb-ch)
     :qu-$  (cq:make-cqueue usb-ch)
     :qu-ln (cq:make-cqueue usb-ch env:*usb-sp-qu-ln-capacity*))))

(defun make-repl-chan ()
  (let ((repl-ch (lp:make-channel)))
    (%make-repl-chan
     :ch     repl-ch
     :qu-0   (cq:make-cqueue repl-ch)
     :qu-msg (cq:make-cqueue repl-ch))))

(defvar ctl-ch  (make-ctl-chan))
(defvar file-ch (make-file-chan))
(defvar usb-ch  (make-usb-chan))
(defvar repl-ch (make-repl-chan))

(defvar ctl-qu-0   (ctl-chan-qu-0   ctl-ch))
(defvar ctl-qu-$   (ctl-chan-qu-$   ctl-ch))
(defvar ctl-qu-job (ctl-chan-qu-job ctl-ch))
(defvar ctl-qu-usb (ctl-chan-qu-usb ctl-ch))

(defvar file-qu-0   (file-chan-qu-0   file-ch))
(defvar file-qu-$   (file-chan-qu-$   file-ch))
(defvar file-qu-usb (file-chan-qu-usb file-ch))

(defvar usb-qu-0   (usb-chan-qu-0  usb-ch))
(defvar usb-qu-?   (usb-chan-qu-?  usb-ch))
(defvar usb-qu-$   (usb-chan-qu-$  usb-ch))
(defvar usb-qu-ln  (usb-chan-qu-ln usb-ch))

(defvar repl-qu-0   (repl-chan-qu-0   repl-ch))
(defvar repl-qu-msg (repl-chan-qu-msg repl-ch))

(defvar +queue-event-msg-table+
  (list (list :ctl-qu-0    (list (list :usb-0   (list :switch))
                                 (list :usb-$   (list #'evt:grbl-$$-msg-p))
                                 (list :usb-err (list #'stringp))
                                 (list :file-0  (list :switch))))
        (list :ctl-qu-$    (list (list :usb-$   (list #'evt:grbl-$$-msg-p))
                                 (list :file-$  (list :done
                                                      #'stringp))))
        (list :ctl-qu-job  nil)                       ; depricate
        (list :ctl-qu-usb  nil)
        (list :usb-qu-?    #'grbl-state-p)            ; special case
        (list :usb-qu-0    (list (list :ctl-0   (list :?
                                                      :$$
                                                      :quit))))
        (list :usb-qu-$    (list (list :ctl-$   (list :?
                                                      :$$
                                                      :abort
                                                      :halt
                                                      :resume
                                                      #'evt:nc-file-msg-p))
                                 (list :file-ln (list :abort
                                                      :halt
                                                      :resume))))
        (list :usb-qu-ln   (list (list :file-ln (list #'evt:line-msg-p))))
        (list :file-qu-0   (list (list :ctl-0   (list :quit))))
        (list :file-qu-$   (list (list :ctl-$   (list #'evt:nc-file-msg-p
                                                      :abort
                                                      :halt
                                                      :resume))))
        (list :file-qu-usb (list (list :usb-msg (list #'evt:line-msg-p))))
        (list :repl-qu-0   (list (list :ctl-0   (list :quit))))
        (list :repl-qu-msg nil)))

(defun %valid-push-p (qu e)
  (let ((l1 (cadr (find-if #'(lambda (i) (eql (car i) qu))
                           +queue-event-msg-table+))))
    (when l1
      (let ((l2 (cadr (find-if #'(lambda (i) (eql (car i) (evt:event-tag e))) l1))))
        (when l2
          (let ((m (evt:event-msg e)))
            (some #'identity
                  (mapcar #'(lambda (i) (if (functionp i) (funcall i m) (eql i m))) l2))))))))

(defun %valid-push-p-2 (qu e)
  (let ((l (assoc qu +queue-event-msg-table+)))
    (when (and (consp l) (evt:event-p e))
      (let ((m (evt:event-msg e)))
        (some #'identity
              (mapcar #'(lambda (i) (if (functionp i) (funcall i m) (eql i m)))
                      (cadr (assoc m l))))))))

(defun send-event (qu e)
  (when (%valid-push-p qu e)
    (case qu
      (:ctl-qu-0    (cq:cqpush ctl-qu-0    e))
      (:ctl-qu-$    (cq:cqpush ctl-qu-$    e))
      (:ctl-qu-job  (cq:cqpush ctl-qu-job  e))
      (:ctl-qu-usb  (cq:cqpush ctl-qu-usb  e))
      (:file-qu-0   (cq:cqpush file-qu-0   e))
      (:file-qu-$   (cq:cqpush file-qu-$   e))
      (:file-qu-usb (cq:cqpush file-qu-usb e))
      (:usb-qu-0    (cq:cqpush usb-qu-0    e))
      (:usb-qu-$    (cq:cqpush usb-qu-$    e))
      (:usb-qu-ln   (cq:cqpush usb-qu-ln   e))
      (:repl-qu-0   (cq:cqpush repl-qu-0   e))
      (:repl-qu-msg (cq:cqpush repl-qu-msg e))
      (otherwise nil))))

(defun %fn-event (qu fn)
  (case qu
    (:ctl-qu-0    (funcall fn ctl-qu-0))
    (:ctl-qu-$    (funcall fn ctl-qu-$))
    (:ctl-qu-job  (funcall fn ctl-qu-job))
    (:ctl-qu-usb  (funcall fn ctl-qu-usb))
    (:file-qu-0   (funcall fn file-qu-0))
    (:file-qu-$   (funcall fn file-qu-$))
    (:file-qu-usb (funcall fn file-qu-usb))
    (:usb-qu-0    (funcall fn usb-qu-0))
    (:usb-qu-$    (funcall fn usb-qu-$))
    (:usb-qu-ln   (funcall fn usb-qu-ln))
    (:repl-qu-0   (funcall fn repl-qu-0))
    (:repl-qu-msg (funcall fn repl-qu-msg))
    (othrwise nil)))

(defun peek-event  (qu) (%fn-event qu #'cq:cqpeek))
(defun peek!-event (qu) (%fn-event qu #'cq:cqpeek!))
(defun empty-qu-p  (qu) (%fn-event qu #'cq:cq-empty-p))
(defun full-qu-p   (qu) (%fn-event qu #'cq:cq-full-p))

(defun grbl-state? (st)
  (when (grbl-state-p st)
    (labels ((ok (len l)
               (and (consp l) (eql len (length l)) (every #'numberp l))))
      (and (member (grbl-state-st st) +grbl-states+)
           (ok 3 (grbl-state-wpos st))
           (ok 2 (grbl-state-bf st))
           (ok 2 (grbl-state-fs st)))
      st)))

(defun maybe-grbl-state ()
  "Observe the current GRBL state without updating it, or NIL on GRBL-STATE type error."
  (grbl-state? (observe (usb-chan-qu-? usb-ch))))

(defun grbl-state ()
  "Observe the current GRBL state without updating it; eventually throw GRBL-STATE-READ-ERROR."
  (let ((st (observe (usb-chan-qu-? usb-ch))))
    (if (grbl-state? st)
        st
        (signal 'grbl-state-read-error))))

(defun grbl-state! (st)
  "Update the observable GRBL state, rolling forward of :SUBSTATE."
  (cond ((grbl-state-p st)
         (let ((usb-qu-? (usb-chan-qu-? usb-ch))
               (prev-st (maybe-grbl-state)))
           (when prev-st
             (setf (grbl-state-substate st) (grbl-state-substate prev-st)))
           (update-observable usb-qu-? st)))
        (t (signal 'grbl-state-update-error))))



